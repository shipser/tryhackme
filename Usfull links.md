# Usfull links

1. [Steganography](https://null-byte.wonderhowto.com/how-to/steganography-hide-secret-data-inside-image-audio-file-seconds-0180936/)
2. [Reverse Google Image Search](https://images.google.com/)
3. [Hide Data In Audio Files](https://null-byte.wonderhowto.com/how-to/hacks-mr-robot-hide-data-audio-files-0164136/)
4. [How to hack forum accounts with password-stealing pictures](https://null-byte.wonderhowto.com/how-to/hack-forum-accounts-with-password-stealing-pictures-0179953/)
5. [Steganography - A list of useful tools and resources](https://0xrick.github.io/lists/stego/)
6. [ExploitDB](https://www.exploit-db.com/) - Search know exploits
7. [NVD](https://nvd.nist.gov/vuln/search) - Search for known CVEs {regardless if ther is an exploit}
8. [CVE Mitre](https://cve.mitre.org/) - CVE DB
9. [SEO Site Checkup](https://seositecheckup.com/)
10. [https://neilpatel.com/seo-analyzer/](https://neilpatel.com/seo-analyzer/)
11. [Nmap videos](https://www.youtube.com/watch?v=AEbVb5xQqzs)
