# Ordered by Tool Name

---

> Writen by Shay Pasvolsky
> sep 15,2020
> To do: Complete 'add_cron_job.sh' and 'add_ssh_key.sh'

1. All of those scripts taken from [JohnHammon](https://github.com/JohnHammond/poor-mans-pentest).
2. Create 'pmp' directory in '/opt'.
3. Add '/opt/pmp' to your rc file.

    ```txt
    for bash:
    ```

    ```bash
    echo "export PATH=$PATH:/opt/pmp" >> ~/.bashrc
    ```

    ```txt
    for zsh:
    ```

    ```bash
    echo "export PATH=$PATH:/opt/pmp" >> ~/.zshrc
    ```

4. For any of this to Work, Put 'functions.sh' in '/opt/pmp/'.
5. Some of this scripts need 'terminator', Install on Kali using the folowing command:

    ```bash
    sudo apt update & sudo apt install terminator
    ```

6. All of the scripts need 'xte', Install on kali using the folowing command:

   ```bash
   sudo apt update & sudo apt install xautomation
   ```

7. All of the scripts need 'guake', install on kali using the folowing command:

    ```bash
    sudo apt update & sudo apt install guake
    ```

8. Update the 'functions.sh' -> 'hide_guake' function with your key bindings for guake.
9. Update the 'functions.sh' -> 'IP' variable with you interface to the internet or the vpn interface used to work.
10. Don't forget to set the ownership for the scripts to your user name.
11. Don't forget to set the scripts to be exaqutibals.
12. Download LinEnum from 'https://github.com/rebootuser/LinEnum' using the command:

    ```bash
    git clone "https://github.com/rebootuser/LinEnum"
    ```

13. Put LinEnum.sh in '/opt/LinEnum', and make it exequtible.

## upload_file_wget.sh

---

1. Open guake {F12}
2. Run Using the command:

    ```bash
    upload_file_wget.sh file_to_upload
    ```

3.The file will upload to '/dev/shm/' on the victim machine.

## upload_file_nc.sh

---

1. Open guake {F12}
2. Run Using the command:

    ```bash
    upload_file_nc.sh file_to_upload
    ```

3.The file will upload to '/dev/shm/' on the victim machine.

## stablize_zsh_shell.sh

---

1. Open guake {F12}
2. Run 'stablize_zsh_shell.sh' while the victim is connected in your default terminal.

## stablize_shell.sh

---

1. Open guake {F12}
2. Run 'stablize_shell.sh' while the victim is connected in your default terminal.

## python_revshell.sh

---

1. For this you need one terminator window for the victim machine.
2. Open guake {F12}
3. Run 'python_revshell.sh'.
4. This script has some different options to run on the victim machine.

## nc_revshell.sh

---

1. For this you need one terminator window for the victim machine.
2. Open guake {F12}
3. Run 'nc_revshell.sh'.

## lenum.sh

---

1. This script needs 'upload_file_nc.sh' to be in '/opt/pmp' directory of the current terminal window.
2. This script need 'LinEnum.sh' to be in '/opt/LinEnum'.
3. Get the 'LinEnum.sh' using the command:

    ```bash
    git clone "https://github.com/rebootuser/LinEnum"
    ```

4. Put the script in it's right destination.
5. The output of the script will be on the victim machine in '/dev/shm/linlog.txt'.
6. Open guake {F12}
7. Run using './lenum.sh'.

## functions.sh

---

1. See the main Section.

## download_file_nc.sh

---

1. Needs 2 parameters:
   1. Victim IP
   2. File Path to save the file downloaded - Has to have the same file name as the downloaded file.
2. Open guake {F12}
3. Run this Command:

    ```bash
    download_file_nc.sh Target_IP File_Path_To_Save
    ```

4. Before running, make sure to be in the victim terminal in the folder having the file needs to be downloaded.

## add_ssh_key.sh

---

## add_cron_job.sh

---
