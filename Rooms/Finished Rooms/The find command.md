# The find command

> Shay Pasvolsky
> Started: 27/09/20 16-27-25
> Finished: 27/09/20 17-00-00

---

---

## General info

```bash
export MIP=
```

### Users

```txt
    User:
        Password:
```

### Open Ports

port:
    type: tcp, udp, tcp6, udp6
    service:
        version:
        Has Vuln:

---

## Tasks

### Task 1

1. Read and follow the instructions.

    ```txt
    Hit 'Completed'
    ```

### Task 2  

1. Find all files whose name ends with ".xml"

    ```bash
    find / -type f -name "*.xml"
    ```

2. Find all files in the /home directory (recursive) whose name is "user.txt" (case insensitive)

    ```bash
    find /home -type f -name user.txt
    ```

3. Find all directories whose name contains the word "exploits"

    ```bash
    find / -type d -name "*exploits*"
    ```

### Task 3

1. Find all files owned by the user "kittycat"

    ```bash
    find / -type f -user kittycat
    ```

2. Find all files that are exactly 150 bytes in size

    ```bash
    find / -type f-size 150c
    ```

3. Find all files in the /home directory (recursive) with size less than 2 KiB’s and extension ".txt"

    ```bash
    find /home -type f -size -2k -name "*.txt"
    ```

4. Find all files that are exactly readable and writeable by the owner, and readable by everyone else (use octal format)

    ```bash
    find / -type f -perm 644
    ```

5. Find all files that are only readable by anyone (use octal format)

    ```bash
    find z -type f -perm /444
    ```

6. Find all files with write permission for the group "others", regardless of any other permissions, with extension ".sh" (use symbolic format)

    ```bash
    find / -type f -perm /o=w -name "*.sh"
    ```

7. Find all files in the /usr/bin directory (recursive) that are owned by root and have at least the SUID permission (use symbolic format)

    ```bash
    find /user/bin -type f -user root -perm -u=s
    ```

8. Find all files that were not accessed in the last 10 days with extension ".png"

    ```bash
    find / -type f -atime -10 -name "*.png"
    ```

9. Find all files in the /usr/bin directory (recursive) that have been modified within the last 2 hours

    ```bash
    find /usr/bin -type f-mmin -120
    ```

### Task 4

1. You are now better equipped to find anything you’re looking for in a filesystem.

    ```bash
    Hit 'Completed'
    ```
