# Inclusion

> Shay Pasvolsky
> Started: 06/10/20 23-22-10
> Finished: 06/10/20 23-48-00

---

---

## General info

```bash
export MIP=10.10.74.220
export MyIP=10.9.134.47
```

### Users

```txt
    User: falconfeast
        Password: rootpassword
```

### Open Ports

```txt
    port:
        type: tcp, udp, tcp6, udp6
        service:
            version:
            Has Vuln:
```

---

## Tasks

### Task 1

1. Deploy the machine and start enumerating

    ```txt
        Hit 'Completed'
    ```

### Task 2

1. user flag?

    ```txt
        60989655118397345799
    ```

2. root flag?

    ```txt
    ```
