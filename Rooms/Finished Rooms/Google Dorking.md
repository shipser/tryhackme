# Google Dorking

> Finished on: sep 16,2020

## Task 1

Hit 'Completed'

## Task 2

1. Name the key term of what a "Crawler" is used to do

    ```txt
    index
    ```

2. What is the name of the technique that "Search Engines" use to retrieve this information about websites?

    ```txt
    crawling
    ```

3. What is an example of the type of contents that could be gathered from a website?

    ```txt
    keywords
    ```

## Task 3

1. Using the SEO Site Checkup tool on "tryhackme.com", does TryHackMe pass the “Meta Title Test”? (Yea / Nay)

    ```txt
    yea
    ```

2. Does "tryhackme.com" pass the “Keywords Usage Test?” (Yea / Nay)

    ```txt
    Nay
    ```

3. Use [https://neilpatel.com/seo-analyzer/](https://neilpatel.com/seo-analyzer/) to analyse [http://googledorking.cmnatic.co.uk](http://googledorking.cmnatic.co.uk):

    ```txt
    Hit 'Completed'
    ```

4. With the same tool and domain in Question #3 (previous): How many pages use “flash”

    ```txt
    0
    ```

5. From a "rating score" perspective alone, what website would list first? tryhackme.com or googledorking.cmnatic.co.uk; Use tryhackme.com's score of 62/100 as of 31/03/2020 for this question.

    ```txt
    googledorking.cmnatic.co.uk
    ```

## Task 4

1. Where would "robots.txt" be located on the domain "ablog.com"

    ```txt
    ablog.com/robots.txt
    ```

2. If a website was to have a sitemap, where would that be located?

    ```txt
    /sitemap.xml
    ```

3. How would we only allow "Bingbot" to index the website?

    ```txt
    User-agent: Bingbot
    ```

4. How would we prevent a "Crawler" from indexing the directory "/dont-index-me/"?

    ```txt
    Disallow: /dont-index-me/
    ```

5. What is the extension of a Unix/Linux system configuration file that we might want to hide from "Crawlers"?

    ```txt
    .conf
    ```

## Task 5

1. What is the typical file structure of a "Sitemap"?

    ```txt
    xml
    ```

2. What real life example can "Sitemaps" be compared to?

    ```txt
    map
    ```

3. Name the keyword for the path taken for content on a website

    ```txt
    route
    ```

## Task 6

1. What would be the format used to query the site bbc.co.uk about flood defences

    ```txt
    site: bbc.co.uk flood defences
    ```

2. What term would you use to search by file type?

    ```txt
    filetype:
    ```

3. What term can we use to look for login pages?

    ```txt
    intitle: login
    ```

---

## Main Tips

1. Find Robots.txt in web sites.
2. Find the SiteMap in web sites.
