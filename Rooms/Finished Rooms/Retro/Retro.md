# Retro

---

> Shay Pasvolsky
> Started: sep 19, 2020
> Finished: sep 19, 2020

## General info

```bash
export MIP=10.10.218.18
```

### Users

1. U: Wade P: parzival

## Tools

1. Gobuster

    ```bash
    sudo gobuster dir -u 10.10.218.18 -o gobuster/scanres -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -t 100
    ```

    ```txt
    /retro
    ```

2. nmap

    ```bash
    sudo nmap -sC -sV -vvv -p- -Pn --script vuln $MIP -oN nmap/results
    ```

    ```txt
    ```

3. xfreerdp

    ```bash
    xfreerdp /v:10.10.218.18 /u:Wade
    ```

## Task 1

1. A web server is running on the target. What is the hidden directory which the website lives on?

    ```txt
    /retro
    ```

2. user.txt

    ```txt
    3b99fbdc6d430bfb51c72c651a261927
    ```

3. root.txt

    ```txt
    7958b569565d7bd88d10c6f22d1c4063
    ```
