# Sublist3r

> Shay Pasvolsky
> Started: 09/10/20 11-59-34
> Finished: 09/10/20 12-33-00

---

---

## General info

```bash
export MIP=
export MyIP=10.9.134.47
```

### Users

```txt
    User:
        Password:
        for what use:
```

### Open Ports

```txt
    port:
        type: tcp, udp, tcp6, udp6
        service:
            version:
            Has Vuln:
```

---

## Tasks

### Task 1

1. You can find Sublist3r here! We'll install this in the next task.

    ```txt
        Hit 'Completed'
    ```

### Task 2

1. First, let's change to our opt directory: cd /opt

    ```bash
        cd /opt
    ```

    ```txt
        Hit 'Completed'
    ```

2. Next, let's clone the Sublist3r repository into opt: git clone <https://github.com/aboul3la/Sublist3r.git>

    ```txt
        Hit 'Completed'
    ```

3. Now let's move into the Sublist3r directory we've just created: cd /opt/Sublist3r

    ```txt
        Hit 'Completed'
    ```

4. Finally, let's install the requirements for running Sublist3r: pip3 install -r requirements.txt

    ```txt
        Hit 'Completed'
    ```

### Task 3

1. What switch can we use to set our target domain to perform recon on?

    ```txt
        -d
    ```

2. How about setting which engines we'll use for searching? (i.e. google, bing, etc)

    ```txt
        -e
    ```

3. Saving our output is important both so we don't have to run recon again but also so we can return to our returns and review them at a later time. What switch do we use to define an output file?

    ```txt
        -o
    ```

4. Sublist3r can sometimes take some time to run but we can speed through up the use of threads. Which switch allows us to set the number of threads?

    ```txt
        -t
    ```

5. Last but not least, we can also bruteforce the domains for our target. This isn't always the most useful, however, it can sometimes find a key domain that we might have missed. What switch allows us to enable brute forcing?

    ```txt
        -b
    ```

### Task 4

1. Let's run sublist3r now against nbc.com, a fairly large American news company. Run this now with the command: python3 sublist3r.py -d nbc.com -o sub-output-nbc.txt

    ```txt
        Hit 'Completed'
    ```

2. Once that completes open up your results and take a look through them. Email domains are almost always interesting and typically have an email portal (usually Outlook) located at them. Which subdomain is likely the email portal?

    ```txt
        mail
    ```

3. Administrative control panels should never be exposed to the internet! Which subdomain is exposed that shouldn't be?

    ```txt
        admin
    ```

4. Company blogs can sometimes reveal information about internal activities, which subdomain has the company blog at it?

    ```txt
        blog
    ```

5. Development sites are often vulnerable to information disclosure or full-blown attacks. Two developer sites are exposed, which one is associated directly with web development?

    ```txt
        dev-www
    ```

6. Customer and employee help desk portals can often reveal internal nomenclature and other potentially sensitive information, which dns record might be a helpdesk portal?

    ```txt
        help
    ```

7. Single sign-on is a feature commonly used in corporate domains, which dns record is directly associated with this feature? Include both parts of this subdomain separated by a period.

    ```txt
        ssologin.stg
    ```

8. One last one for fun. NBC produced a popular sitcom about typical office work environment, which dns record might be associated with this show?

    ```txt
        office-words
    ```
