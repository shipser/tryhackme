# Tutorial

> Shay Pasvolsky
> Started: 05/10/20 14-38-10
> Finished: 05/10/20 14-41-00

---

---

## General info

```bash
export MIP=10.10.28.97
export MyIP=10.9.134.47
```

### Users

```txt
    User:
        Password:
```

### Open Ports

```txt
    port:
        type: tcp, udp, tcp6, udp6
        service:
            version:
            Has Vuln:
```

---

## Tasks

### Task 1

1. What is the flag text shown on the website of the machine you deployed on this task?

    ```txt
        flag{connection_verified}
    ```

### Task 2

1. Read the above and explore TryHackMe!

    ```txt
        Hit 'Completed'
    ```
