# **Introductory Researching**

---

> Finished on sep 15,2020

- press the [Join room](http://#/) button to use the machins in this Stage.

## **Task 1 (Introduction)**

---

### **Read the Introduction**

```txt
No asnwer neede
```

press the [completed](http://#/) button after reading.

## **Task 2 (Example Research Question)**

---

**reading the artical on [Steganography](https://null-byte.wonderhowto.com/how-to/steganography-hide-secret-data-inside-image-audio-file-seconds-0180936/)**

**_Tools in this artical:_**

### steghide (used in linux)

- used like so:

```bash
steghide -ef path_to_file_needed_to_be_hidden -cf path_to_file_to_hide_inside -sf output_file_name-optional -z compression_level_from_1_to_9-If_capital_no_compression_will_be_used -e ecription_type-can_be_none_or_any_seported_encrition-If_omited_then_deafults_to-128bit_AES
```

```txt
you will be asked to set a password for the file.
```

- to extract use the command:

```bash
steghide extract -sf stegoFile -xf outputFile
```

```txt
you will need the password from the encryption procces.
```

- install with the command:

```bash
apt install steghide
```

### Reverse google image search - [link](https://images.google.com/)

### Some more reading

- [hide data in audio files](https://null-byte.wonderhowto.com/how-to/hacks-mr-robot-hide-data-audio-files-0164136/)

- [How to hack forum accounts with password-stealing pictures.](https://null-byte.wonderhowto.com/how-to/hack-forum-accounts-with-password-stealing-pictures-0179953/)

- [Need to go into it](https://0xrick.github.io/lists/stego/)

### **Install the steghide tool (did not do it, need a linux machine):**

```bash
apt install steghide
```

### Questions to fill

1. In the Burp Suite Program that ships with Kali Linux, what mode would you use to manually send a request (often repeating a captured request numerous times)?

    ```txt
    Repeater
    ```

2. What hash format are modern Windows login passwords stored in?

    ```txt
    NTLM
    ```

3. What are automated tasks called in Linux?

    ```txt
    cron jobs
    ```

4. What number base could you use as a shorthand for base 2 (binary)?

    ```txt
    base 16
    ```

5. If a password hash starts with $6$, what format is it (Unix variant)?

    ```txt
    Sha512crypt
    ```

## **Task 3 (Vulnerability Searching)**

---

**_Usfull Links:_**

1. [ExploitDB](https://www.exploit-db.com/)
2. [NVD](https://nvd.nist.gov/vuln/search)
3. [CVE Mitre](https://cve.mitre.org/)

**_Usfull Tools:_**

### searchsploit

example:

```bash
searchsploit fuelcms
```

### Questions to fill in Task 3

1. What is the CVE for the 2020 Cross-Site Scripting (XSS) vulnerability found in WPForms?

    ```txt
    CVE-2020-10385
    ```

2. There was a Local Privilege Escalation vulnerability found in the Debian version of Apache Tomcat, back in 2016. What's the CVE for this vulnerability?

    ```txt
    CVE-2016-1240
    ```

3. What is the very first CVE found in the VLC media player?

    ```txt
    CVE-2007-0017
    ```

4. If I wanted to exploit a 2020 buffer overflow in the sudo program, which CVE would I use?

    ```txt
    CVE-2019-18634
    ```

## **Task 4 (Manual Pages)**

---

- Go to [Learn Linux Room](https://tryhackme.com/room/zthlinux) To learn some linux.

### Questions to fill in Task 4

1. SCP is a tool used to copy files from one computer to another. What switch would you use to copy an entire directory?

    ```txt
    -r
    ```

2. fdisk is a command used to view and alter the partitioning scheme used on your hard drive. What switch would you use to list the current partitions?

    ```txt
    -l
    ```

3. nano is an easy-to-use text editor for Linux. There are arguably better editors (Vim, being the obvious choice); however, nano is a great one to start with. What switch would you use to make a backup when opening a file with nano?

    ```txt
    -B
    ```

4. Netcat is a basic tool used to manually send and receive network requests. What command would you use to start netcat in listen mode, using port 12345?

    ```txt
    nc -l -p 12345
    ```

## **Task 5 (Final Thoughts)**

---

### Questions to fill in Task 5

1. Reaserch coplete - no Answer needed.
