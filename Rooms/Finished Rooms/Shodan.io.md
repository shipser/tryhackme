# Shodan.io

> Shay Pasvolsky
> Started: 28/09/20 11-46-32
> Finished: 28/09/20 12-40-00

---

---

## General info

```bash
export MIP=
export MyIP=10.9.134.47
```

### Users

```txt
    User:
        Password:
```

### Open Ports

port:
    type: tcp, udp, tcp6, udp6
    service:
        version:
        Has Vuln:

---

## Tasks

### Task 1

1. Go to Shodan.io

    ```txt
    Hit 'Completed'
    ```

### Task 2

1. What is Google's ASN number?

    ```txt
    AS15169
    ```

2. When was it allocated? Give the year only.

    ```txt
    2000
    ```

3. Where are most of the machines on this ASN number, physically in the world?

    ```txt
    United States
    ```

4. What is Google's top service across all their devices on this ASN?

    ```txt
    ssh
    ```

5. What SSH product does Google use?

    ```txt
    openssh
    ```

6. What is Google's most used Google product, according to this search? Ignore the word "Google" in front of it.

    ```txt
    cloud
    ```

### Task 3

1. Wow, that's nifty!

    ```txt
    Hit 'Completed'
    ```

### Task 4

1. What is the top operating system for MYSQL servers in Google's ASN?

    ```txt
    5.6.40-84.0-log
    ```

2. What is the 2nd most popular country for MYSQL servers in Google's ASN?

    ```txt
    Netherlands
    ```

3. Under Google's ASN, which is more popular for nginx, Hypertext Transfer Protocol or Hypertext Transfer Protocol(s)?

    ```txt
    Hypertext Transfer Protocol
    ```

4. Under Google's ASN, what is the most popular city?

    ```txt
    Mountain View
    ```

5. Under Google's ASN in Los Angeles, what is the top operating system according to Shodan?

    ```txt
    pan-os
    ```

6. Using the top Webcam search from the explore page, does Google's ASN have any webcams? Yay / nay.

    ```txt
    nay
    ```

### Task 5

1. Read the blog post above!

    ```txt
    Hit 'Completed'
    ```
