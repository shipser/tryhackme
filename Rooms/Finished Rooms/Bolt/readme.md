# Bolt

> Shay Pasvolsky
> Started: 27/09/20 22-16-57
> Finished: 27/09/20 23-03-00

---

---

## General info

```bash
export MIP=10.10.181.141
export MyIP=10.9.134.47
```

### Users

```txt
    User: bolt
        Password: boltadmin123

    User: jake
        Password:

    User: admin
        Password:
```

### Open Ports

port: 22
    type: tcp
    service:
        version:
        Has Vuln:

port: 80
    type: tcp
    service:
        version:
        Has Vuln:

port: 8000
    type: tcp
    service:
        version:
        Has Vuln:

---

## Tasks

### Task 1

1. Start the machine

    ```txt
    Hit 'Completed'
    ```

### Task 2

1. What port number has a web server with a CMS running?

    ```bash
    sudo nmap -sC -sV -vvv --script vuln $MIP -oN nmap/results
    ```

    ```txt
    8000
    ```

2. What is the username we can find in the CMS?

    ```txt
    bolt
    ```

3. What is the password we can find for the username?

    ```txt
    boltadmin123
    ```

4. What version of the CMS is installed on the server? (Ex: Name 1.1.1)

    ```txt
    Go to login page (/bolt/login)
    enter username and password
    at the butoom - bolt 3.7.1
    ```

5. There's an exploit for a previous version of this CMS, which allows authenticated RCE. Find it on Exploit DB. What's its EDB-ID?

    ```txt
    48296
    ```

6. Metasploit recently added an exploit module for this vulnerability. What's the full path for this exploit? (Ex: exploit/....)

    Note: If you can't find the exploit module its most likely because your metasploit isn't updated. Run `apt update` then `apt install metasploit-framework`

    ```txt
    exploit/unix/webapp/bolt_authenticated_rce
    ```

7. Set the LHOST, LPORT, RHOST, USERNAME, PASSWORD in msfconsole before running the exploit

    ```txt
    hit 'Completed'
    ```

8. Look for flag.txt inside the machine.

    ```txt
    THM{wh0_d035nt_l0ve5_b0l7_r1gh7?}
    ```
