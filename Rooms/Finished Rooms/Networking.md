# Networking

> Shay Pasvolsky
> Started: 27/09/20 20-39-38
> Finished: 27/09/20 22-11-00

---

---

## General info

```bash
export MIP=
export MyIP=10.9.134.47
```

### Users

```txt
    User:
        Password:
```

### Open Ports

port:
    type: tcp, udp, tcp6, udp6
    service:
        version:
        Has Vuln:

---

## Tasks

### Task 1

1. How many categories of IPv4 addresses are there?

    ```txt
    5
    ```

2. Which type is for research? *Looking for a letter rather than a number here

    ```txt
    E
    ```

3. How many private address ranges are there?

    ```txt
    3
    ```

4. Which private range is typically used by businesses?

    ```txt
    A
    ```

5. There are two common default private ranges for home routers, what is the first one?

    ```txt
    192.168.0.0
    ```

6. How about the second common private home range?

    ```txt
    192.168.1.0
    ```

7. How many addresses make up a typical class C range? Specifically a /24

    ```txt
    256
    ```

8. Of these addresses two are reserved, what is the first addresses typically reserved as?

    ```txt
    network
    ```

9. The very last address in a range is typically reserved as what address type?

    ```txt
    broadcast
    ```

10. A third predominant address type is typically reserved for the router, what is the name of this address type?

    ```txt
    gateway
    ```

11. Which address is reserved for testing on individual computers?

    ```txt
    127.0.0.1
    ```

12. A particularly unique address is reserved for unroutable packets, what is that address? This can also refer to all IPv4 addresses on the local machine.

    ```txt
    0.0.0.0
    ```

## Task 2

1. 1001 0010

    ```txt
    146
    ```

2. 0111 0111

    ```txt
    119
    ```

3. 1111 1111

    ```txt
    255
    ```

4. 1100 0101

    ```txt
    197
    ```

5. 1111 0110

    ```txt
    246
    ```

6. 0001 0011

    ```txt
    19
    ```

7. 1000 0001

    ```txt
    129
    ```

8. 0011 0001

    ```txt
    49
    ```

9. 0111 1000

    ```txt
    120
    ```

10. 1111 0000

    ```txt
    240
    ```

11. 0011 1011

    ```txt
    59
    ```

12. 0000 0111

    ```txt
    7
    ```

## Task 3

1. 238

    ```txt
    238-128=110 -> 1000 0000
    110-64=46   -> 1100 0000
    46-32=14    -> 1110 0000
    14-8=6      -> 1110 1000
    6-4=2       -> 1110 1100
    2-2=0       -> 1110 1110

    11101110
    ```

2. 34

    ```txt
    34-32=2
    2-2=0

    00100010
    ```

3. 123

    ```txt
    0*128+1*64+1*32+1*16+1*8+0*4+1*2+1*1=64+32+16+8+2+1=96+24+3=120+3=123

    01111011
    ```

4. 50

    ```txt
    0*128+0*64+1*32+1*16+0*8+0*4+1*2+0*1=32+16+2=48+2=50

    00110010
    ```

5. 255

    ```txt
    11111111
    ```

6. 200

    ```txt
    1*128+1*64+0*32+0*16+1*8+0*4+0*2+0*1=128+64+8=192+8=200

    11001000
    ```

7. 10

    ```txt
    00001010
    ```

8. 138

    ```txt
    10001010
    ```

9. 1

    ```txt
    00000001
    ```

10. 13

    ```txt
    00001101
    ```

11. 250

    ```txt
    11111010
    ```

12. 114

    ```txt
    01110010
    ```

## Task 4

1. 10.240.1.1

    ```txt
    A
    ```

2. 150.10.15.0

    ```txt
    B
    ```

3. 192.14.2.0

    ```txt
    C
    ```

4. 148.17.9.1

    ```txt
    B
    ```

5. 193.42.1.1

    ```txt
    C
    ```

6. 126.8.156.0

    ```txt
    A
    ```

7. 220.200.23.1

    ```txt
    C
    ```

8. 230.230.45.58

    ```txt
    D
    ```

9. 177.100.18.4

    ```txt
    B
    ```

10. 119.18.45.0

    ```txt
    A
    ```

11. 117.89.56.45

    ```txt
    A
    ```

12. 215.45.45.0

    ```txt
    C
    ```
