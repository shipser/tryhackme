# Crack the hash

> Shay Pasvolsky
> Started: 05/10/20 14-43-49
> Finished: 05/10/20 15-51-00

---

---

## General info

```bash
export MIP=
export MyIP=10.9.134.47
```

### Users

```txt
    User:
        Password:
```

### Open Ports

```txt
    port:
        type: tcp, udp, tcp6, udp6
        service:
            version:
            Has Vuln:
```

---

## Tasks

### Task 1

1. 48bb6e862e54f2a795ffc4e541caed4d

    ```firefox
        http://crackstation.net
    ```

    ```txt
        easy
    ```

2. CBFDAC6008F9CAB4083784CBD1874F76618D2A97

    ```txt
        password123
    ```

3. 1C8BFE8F801D79745C4631D09FFF36C82AA37FC4CCE4FC946683D7B336B63032

    ```txt
        letmein
    ```

4. $2y$12$Dwt1BZj6pcyc3Dy1FWZ5ieeUznr71EeNkJkUlypTsgbX1H68wsRom

    ```bash
        hashcat -m 3200 hash.hash /usr/share/wordlists/rockyou.txt
    ```

    ```txt
        bleh
    ```

5. 279412f945939ba78ce0758d3fd83daa

    ```txt
        Eternity22
    ```

### Task 2

1. Hash: F09EDCB1FCEFC6DFB23DC3505A882655FF77375ED8AA2D1C13F640FCCC2D0C85

    ```txt
        paule
    ```

2. Hash: 1DFECA0C002AE40B8619ECF94819CC1B

    ```txt
        n63umy8lkf4i
    ```

3. Hash: $6$aReallyHardSalt$6WKUTqzq.UQQmrm0p/T7MPpMbGNnzXPMAXi4bJMl9be.cfi3/qxIf.hsGpS41BqMhSrHVXgMpdjS6xeKZAs02.

    Salt: aReallyHardSalt

    Rounds: 5

    ```txt
        waka99
    ```

4. Hash: e5d8870e5bdd26602cab8dbe07a942c8669e56d6

    Salt: tryhackme

    ```bash
        hashcat -m 160 ...
    ```

    ```txt
        481616481616
    ```
