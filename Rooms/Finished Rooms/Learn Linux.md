# Learn Linux

---

> Finished on sep 15,2020

## General Data

Machine ip: 10.10.94.208

```bash
export MIP=10.10.94.208
```

Username: shiba1, Password: shiba1
Username: shiba2, Password: pinguftw
Username: shiba3, Password: happynootnoises
Username: shiba4, Password: test1234
Username: noot, Password: noot
Username: nootnoot, Password: notsofast

## Task 1

Hit 'Deploy' and 'Completed'

## Task 2

Hit 'Completed'

## Task 3

Hit 'Completed'

## Task 4

```bash
ssh shiba1@MIP
yes
shiba1
```

Hit 'Completed'

## Task 5

```bash
echo hello
```

Hit 'Completed'

## Task 6

```bash
man echo
q
echo -n shiba
```

1. How would you output hello without a newline?

   ```txt
   echo -n hello
   ```

Hit 'Submit'

## Task 7

1. What flag outputs all entries?

    ```txt
    -a
    ```

2. What flag outputs things in a "long list" format?

    ```txt
    -l
    ```

## Task 8

1. What flag numbers all output lines?

    ```txt
    -n
    ```

## Task 9

Hit 'Completed'

## Task 10

1. How would you run a binary called hello using the directory shortcut . ?

    ```bash
    ./hello
    ```

2. How would you run a binary called hello in your home directory using the shortcut ~ ?

    ```bash
    ~/hello
    ```

3. How would you run a binary called hello in the previous directory using the shortcut .. ?

    ```bash
    ../hello
    ```

## Task 11

```bash
touch noot.txt
./shiba1
```

1. What's the password for shiba2?

    ```txt
    pinguftw
    ```

## Task 12

1. How do you specify which shell is used when you login?

    ```bash
    -s
    ```

## Task 13

Hit 'Completed'

## Task 14

1. How would you output twenty to a file called test?

    ```bash
    echo twenty > test
    ```

## Task 15

Hit 'Completed'

## Task 16

Hit 'Completed'

## Task 17

Hit 'Completed'

## Task 18

1. How would you set nootnoot equal to 1111?

    ```bash
    export nootnoot=1111
    ```

2. What is the value of the home environment variable?

    ```bash
    echo $HOME
    ```

    ```txt
    /home/shiba2
    ```

## Task 19

Hit 'Completed'

## Task 20

Hit 'Completed'

## Task 21

1. What is shiba3's password?

    ```bash
    cd ~
    export test1234=$USER && ./shiba2
    ```

    ```txt
    happynootnoises
    ```

## Task 22

Hit 'Completed'

## Task 23

Hit 'Completed'

## Task 24

1. What permissions mean the user can read the file, the group can read and write to the file, and no one else can read, write or execute the file?

    ```text
    460
    ```

2. What permissions mean the user can read, write, and execute the file, the group can read, write, and execute the file, and everyone else can read, write, and execute the file.

    ```text
    777
    ```

## Task 25

1. How would you change the owner of file to paradox?

    ```bash
    chown paradox:paradox file
    ```

2. What about the owner and the group of file to paradox?

    ```bash
    chown paradox:paradox file
    ```

3. What flag allows you to operate on every file in the directory at once?

    ```text
    -r
    ```

## Task 26

1. What flag deletes every file in a directory?

    ```text
    -r
    ```

2. How do you suppress all warning prompts?

    ```text
    -f
    ```

## Task 27

1. How would you move file to /tmp?

    ```bash
    mv file /tmp
    ```

## Task 28

Hit 'Completed'

## Task 29

1. Using relative paths, how would you cd to your home directory.

    ```bash
    cd ~
    ```

2. Using absolute paths how would you make a directory called test in /tmp

    ```bash
    mkdir /tmp/test
    ```

## Task 30

1. How would I link /home/test/testfile to /tmp/test

    ```bash
    ln /home/test/testfile /tmp/test
    ```

## Task 31

1. How do you find files that have specific permissions?

    ```bash
    -perm
    ```

2. How would you find all the files in /home?

    ```bash
    find /home
    ```

3. How would you find all the files owned by paradox on the whole system?

    ```bash
    find / -user paradox
    ```

## Task 32

1. What flag lists line numbers for every string found?

    ```text
    -n
    ```

2. How would I search for the string boop in the file aaaa in the directory /tmp

    ```bash
    grep boop /tmp/aaaa
    ```

## Task 33

1. What is shiba4's password?

    ```bash
    find / grep shiba4 > shay
    cat shay
    ```

    ```text
    /opt/secret/shiba4
    /home/shiba4
    /home/shiba4/.profile
    /home/shiba4/.bashrc
    /home/shiba4/.bash_logout
    /etc/shiba/shiba4
    ```

    ```bash
    /opt/secret/shiba4
    ```

    ```text
    test1234
    ```

## Task 34

Hit 'Completed'

## Task 35

1. How do you specify which user you want to run a command as.

    ```text
    -u
    ```

2. How would I run whoami as user jen?

    ```bash
    sudo -u jen whoami
    ```

3. How do you list your current sudo privileges(what commands you can run, who you can run them as etc.)

    ```text
    -l
    ```

## Task 36

1. How would I add the user test to the group test?

    ```bash
    sudo usermod -a -G test test
    ```

## Task 37

Hit 'Completed'

## Task 38

Hit 'Completed'

## Task 39

Hit 'Completed'

## Task 40

Hit 'Completed'

## Task 41

Hit 'Completed'

## Task 42

Hit 'Completed'

## Task 43

```bash
ls -l
find / -user shiba2 -type f 2>>/dev/null
```

```text
/var/log/test1234
```

```bash
cat /var/log/test1234
```

```text
notsofast
```

```bash
su nootnoot
notsofast
sudo cat /root/root.txt
```

```txt
ad91979868d06e19d8e8a9c28be56e0c
```
