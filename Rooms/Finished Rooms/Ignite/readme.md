# Ignite

> Shay Pasvolsky
> Started: 06/10/20 15-03-57
> Finished: 06/10/20 16-50-00

---

---

## General info

```bash
    export MIP=10.10.101.110
    export MyIP=10.9.134.47
```

### Users

```txt
    for what: Fuel cms admin
            User: admin
            Password: admin
    for the machine:
            User: root
            Password: mememe
```

### Open Ports

```txt
    port: 80
        type: tcp
        service: Apache
            version: 2.4.18
            Has Vuln: alot
```

### CMS

```txt
    Fuel CMS Version 1.4 on port 80
        admin page on /fuel
    has robots.txt -> disallows: /fuel/ - can be accessed...... -> points to admin page
    uses jquery version 1.7.1
```

---

## Tasks

### Task 1

1. User.txt -> flag.txt

    ```txt
        6470e394cbf6dab6a91682cc8585059b
    ```

2. root.txt

    ```txt
        b9bbcb33e11b80be759c4e844862482d
    ```
