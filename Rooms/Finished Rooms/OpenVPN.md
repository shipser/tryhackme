# OpenVPN

> Shay Pasvolsky, Started on: sep 19,2020.
> Finished on: sep 19,2020

## Task 1

Hit 'Join Room'
Hit 'Completed'

## Task 2

Hit 'Completed'

## Task 3

Hit 'Completed'

## Task 4

Hit 'Completed'

## Task 5

Hit 'Completed'

## Task 6

Hit 'Deploy'

```bash
export MIP=10.10.218.166
```

1. What is the flag displayed on the deployed machine's website?

    ```txt
    flag{connection_verified}
    ```
