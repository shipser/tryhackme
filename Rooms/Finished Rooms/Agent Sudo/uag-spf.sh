#!/bin/bash

MIP=10.10.72.156
mess=""
echo -e "$mess" > scans/local/User-Agent\ spoofing/agents
for l in {{a..z},{A..Z}}
do
    mess="Agent name: ${l}\n-----------------\n"
    mess=${mess}`curl -A $l -L $MIP`
    mess=${mess}"\n\n\n"
    echo -e "$mess" >> scans/local/User-Agent\ spoofing/agents
done