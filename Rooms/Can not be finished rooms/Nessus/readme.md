# Nessus

> Shay Pasvolsky
> Started: 28/09/20 11-11-59
> Finished: Cannot be finished without paying for the suite

---

---

## General info

```bash
export MIP=10.10.84.0
export MyIP=10.9.134.47
```

### Users

```txt
    User:
        Password:
```

### Open Ports

port:
    type: tcp, udp, tcp6, udp6
    service:
        version:
        Has Vuln:

---

## Tasks

### Task 1

1. Deploy the virtual machine!

    ```txt
    Hit 'Completed'
    ```

### Task 2

1. First, create a basic Ubuntu box (or any other system of your choice). Minimum 4 2GHz cores, 4 GB RAM (8 Recommended) and 30 GB of disk space.

    ```txt
    Hit 'Completed'
    ```

2. Next, go ahead and register for a Nessus Home license. This can be used to scan up to 16 IP addresses at a time. Be sure to keep this license information safe, you'll need it for any manual work. Here's the registration link: <https://www.tenable.com/products/nessus-home>

    ```txt
    Hit 'Completed'

    The Code will be in the email you got
    ```

3. Follow the installation instructions on Tenable's website, once Nessus is set up connect the machine that it lives on to the network using your VPN file.

    ```txt
    1. go to the 'Support' manu
    2. go to the 'Documantion' option
    3. Search for install guid

    Hit 'Completed'
    ```

### Task 3

1. As we log into Nessus, we are greeted with a button to launch a scan, what is the name of this button?

    ```bash
    ```

    ```txt
    ```

2. Nessus allows us to create custom templates that can be used during the scan selection as additional scan types, what is the name of the menu where we can set these?

    ```bash
    ```

    ```txt
    ```

3. Nessus also allows us to change plugin properties such as hiding them or changing their severity, what menu allows us to change this?

    ```bash
    ```

    ```txt
    ```

4. Nessus can also be run through multiple 'Scanners' where multiple installations can work together to complete scans or run scans on remote networks, what menu allows us to see all of these installations?

    ```bash
    ```

    ```txt
    ```

5. Let's move onto the scan types, what scan allows us to see simply what hosts are 'alive'?

    ```bash
    ```

    ```txt
    ```

6. One of the most useful scan types, which is considered to be 'suitable for any host'?

    ```bash
    ```

    ```txt
    ```

7. Following a few basic scans, it's often useful to run a scan wherein the scanner can authenticate to systems and evaluate their patching level. What scan allows you to do this?

    ```bash
    ```

    ```txt
    ```

8. When performing Web App tests it's often useful to run which scan? This can be incredibly useful when also using nitko, zap, and burp to gain a full picture of an application.

    ```bash
    ```

    ```txt
    ```

### Task 4

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 5

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 6

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```
