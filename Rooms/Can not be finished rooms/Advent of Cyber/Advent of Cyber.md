# Advent of Cyber

> Shay Pasvolsky
> Started: sep 20, 2020
> Finished: Cannot be finished, conatins many bugs

---

---

## General info

```bash
export MIP=10.10.57.0
```

```txt
web address -> http://$MIP:3000
```

### Users

```txt
    User: admin
        Password: defaultpass
```

### Open Ports

port: 22
    type: tcp
    service: ssh
        version: OpenSSH 7.4 (protocol 2.0)
        Has Vuln: yes
port: 111
    type: tcp, udp, tcp6, udp6
    service: rpcbind
        version: 2-4
        Has Vuln: unkown
port: 3000
    type: tcp
    service: http
        version: Node.js Express framework
        Has Vuln: unkown

### Folders in the web UI

/register (Status: 200)     /Register (Status: 200)
/login (Status: 200)        /Login (Status: 200)
/admin (Status: 302)        /Admin (Status: 302)
/assets (Status: 301)
/css (Status: 301)
/js (Status: 301)
/logout (Status: 302)       /Logout (Status: 302)
/LogIn (Status: 200)        /LOGIN (Status: 200)
/sysadmin (manually found with help)

---

## Tasks

### Task 1

1. Read the above.

    ```txt
    Hit 'Completed'
    ```

### Task 2

1. Practise connecting to our network.

    ```txt
    Hit 'Completed'
    ```

### Task 3

1. Read the above

    ```txt
    Hit 'Completed'
    ```

### Task 4

1. Join the Discord server and say hi!

    ```txt
    Hit 'Completed'
    ```

2. Follow us on Twitter.

    ```txt
    Hit 'Completed'
    ```

### Task 5

1. Read the above

    ```txt
    Hit 'Completed'
    ```

### Task 6

```txt
Hit 'Deploy'
```

1. What is the name of the cookie used for authentication?

    ```txt
    authid
    ```

2. If you decode the cookie, what is the value of the fixed part of the cookie?

    ```txt
    coockie decodes:
        adminv4er9ll1!ss
        santav4er9ll1!ss
    ```

    ```txt
    v4er9ll1!ss
    ```

3. After accessing his account, what did the user mcinventory request?

    ```txt
    set the cookie value to: bWNpbnZlbnRvcnl2NGVyOWxsMSFzcw==
    set it using firefox storege inspector
    ```

    ```txt
    firewall
    ```

### Task 7

1. What is the path of the hidden page?

    ```txt
    /sysadmin
    ```

2. What is the password you found?

    ```txt
    defaultpass
    ```

3. What do you have to take to the 'partay'

    ```bash
    ```

    ```txt
    ```

### Task 8

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 9

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 10

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 11

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 12

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 13

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 14

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 15

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 16

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 17

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 18

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 19

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 20

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 21

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 22

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 23

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 24

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 25

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 26

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 27

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 28

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 29

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```

### Task 30

1. a

    ```bash
    ```

    ```txt
    ```

2. a

    ```bash
    ```

    ```txt
    ```
