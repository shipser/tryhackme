# **[TryHackMe](www.tryhackme.com)**

---

## **Rooms:**

---

### **Completed Rooms:**

1. welcome {Done in two versions}
2. Introductory Researching
3. Learn Linux
4. Google Dorking
5. Nmap
6. OpenVPN
7. Metasploit
8. Ice
9. Blaster
10. Retro
11. Basic Pentesting
12. tmux
13. The find command
14. Networking
15. Bolt
16. Shodan.io
17. Hydra
18. The Cod Caper
19. Vulnversity
20. Overpass 2 - Hacked
21. CC: Pen Testing
22. Kenobi
23. Tutorial
24. Crack the hash
25. Ignite
26. Inclusion
27. Tartarus
28. Agent Sudo
29. Sublist3r

### **Joined but not yet started Rooms:**

1. Blue
2. OWASP Top 10
3. Mr Robot CTF
4. Linux PrivEsc
5. PS Empire
6. Buffer Overflow Prep
7. MAL: Malware Introductory
8. Post-Exploitation Basics
9. Windows PrivEsc Arena
10. Lian_Yu
11. OhSINT
12. Simple CTF
13. Bounty Hacker
14. c4ptur3-th3-fl4g
15. Pickle Rick
16. Introduction to OWASP ZAP
17. Injection
18. OWASP Juice Shop

### **Started but need some work to be completed Rooms:**

1. None

### **Cannot be finished:**

1. Advent of Cyber - rejoind, will give another try
2. Nessus - rejoind, will give another try

### **Paid Rooms To Consider:**

1. [XXE](https://tryhackme.com/why-subscribe)

---

## Pathes

---

### **Completed Pathes:**

1. none

### **Joined Pathes:**

1. none
